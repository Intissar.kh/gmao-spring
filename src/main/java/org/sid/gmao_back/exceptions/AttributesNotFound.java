package org.sid.gmao_back.exceptions;

public class AttributesNotFound extends Exception {

    public AttributesNotFound() {
    }

    public AttributesNotFound(String message) {
        super(message + " : introuvable");
    }

    public AttributesNotFound(Throwable cause) {
        super(cause);
    }

    public AttributesNotFound(String message, Throwable cause) {
        super(message, cause);
    }

    public AttributesNotFound(String message, Throwable cause,
                              boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
