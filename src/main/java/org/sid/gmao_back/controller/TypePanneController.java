package org.sid.gmao_back.controller;


import org.sid.gmao_back.entities.TypeMaintenance;
import org.sid.gmao_back.entities.TypePanne;
import org.sid.gmao_back.service.TypePanneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/typePanne")
public class TypePanneController {

    @Autowired
    private TypePanneService typePanneService;
    @RequestMapping(method = RequestMethod.GET, value = "/list" )
    @ResponseBody
    public List<TypePanne> findAll(){ return typePanneService.findAll();}

    @RequestMapping(method = RequestMethod.GET, value = "/list/{id}")
    @ResponseBody
    public TypePanne findById(@PathVariable(name = "id") Long id){ return typePanneService.findById(id);}

    @RequestMapping(method = RequestMethod.POST, value = "/list")
    @ResponseBody
    public TypePanne save(@RequestBody TypePanne typePanne)
    {
        return typePanneService.save(typePanne);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/list/{id}")
    @ResponseBody
    public TypePanne update(@PathVariable(name = "id") Long id, @RequestBody TypePanne typePanne)
    {
        return typePanneService.update(id,typePanne);
    }

    @RequestMapping(method = RequestMethod.DELETE,value ="/list/{id}")
    @ResponseBody
    public void delete(@PathVariable(name = "id") Long id){typePanneService.delete(id);}

}
