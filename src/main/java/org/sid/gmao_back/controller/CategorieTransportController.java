package org.sid.gmao_back.controller;

import org.sid.gmao_back.entities.CategorieTransport;
import org.sid.gmao_back.entities.CategorieVehicule;
import org.sid.gmao_back.service.CategorieTransportService;
import org.sid.gmao_back.service.CategorieVehiculeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/categorieTransport")
public class CategorieTransportController {
    @Autowired
    private CategorieTransportService categorieTransportService;

    @RequestMapping(method = RequestMethod.GET, value = "/list" )
    @ResponseBody
    public List<CategorieTransport> findAll(){ return categorieTransportService.findAll();}

    @RequestMapping(method = RequestMethod.GET, value = "/list/{id}")
    @ResponseBody
    public CategorieTransport findById(@PathVariable(name = "id") Long id){ return categorieTransportService.findById(id);}

    @RequestMapping(method = RequestMethod.POST, value = "/list")
    @ResponseBody
    public CategorieTransport save(@RequestBody CategorieTransport categorieTransport)
    {
        return categorieTransportService.save(categorieTransport);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/list/{id}")
    @ResponseBody
    public CategorieTransport update(@PathVariable(name = "id") Long id, @RequestBody CategorieTransport categorieTransport)
    {
        return categorieTransportService.update(id,categorieTransport);
    }

    @RequestMapping(method = RequestMethod.DELETE,value ="/list/{id}")
    @ResponseBody
    public void delete(@PathVariable(name = "id") Long id){categorieTransportService.delete(id);}

}
