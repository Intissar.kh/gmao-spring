package org.sid.gmao_back.controller;

import org.sid.gmao_back.entities.BadgeChauffeur;
import org.sid.gmao_back.entities.Chauffeur;
import org.sid.gmao_back.entities.TypeBadge;
import org.sid.gmao_back.service.BadgeChauffeurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/badgeChauffeur")
@CrossOrigin("*")

public class BadgeChauffeurController {
    @Autowired
    private BadgeChauffeurService badgeChauffeurService;

    @RequestMapping(method = RequestMethod.GET, value = "/list" )
    @ResponseBody
    public List<BadgeChauffeur> findAll(){ return badgeChauffeurService.findAll();}

    @RequestMapping(method = RequestMethod.GET, value = "/list/{id}")
    public BadgeChauffeur findById(@PathVariable(name = "id") Long id){ return badgeChauffeurService.findBydId(id);}

    @RequestMapping(method = RequestMethod.POST,consumes = {
            MediaType.APPLICATION_JSON_VALUE},produces = {MediaType.APPLICATION_JSON_VALUE}, value = "/list")

    public ResponseEntity save(@RequestBody BadgeChauffeur badgeChauffeur)
    {
        return new ResponseEntity(badgeChauffeurService.save(badgeChauffeur), HttpStatus.OK );
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/list/{id}")
    public BadgeChauffeur update(@PathVariable(name = "id") Long id, @RequestBody BadgeChauffeur badgeChauffeur)
    {
        return badgeChauffeurService.update(id,badgeChauffeur);
    }

    @RequestMapping(method = RequestMethod.DELETE,value ="/list/{id}")
    public void delete(@PathVariable(name = "id") Long id){badgeChauffeurService.delete(id);}
    @RequestMapping(method = RequestMethod.GET,value = "/typeBadge")
    public List<TypeBadge> afficherBadge(){
        return (List<TypeBadge>) badgeChauffeurService.typeBadge();
    }
    @RequestMapping(method = RequestMethod.GET,value = "/chauffeur")
    public List<Chauffeur> afficherChauffeur(){
        return (List<Chauffeur>) badgeChauffeurService.chauffeur();
    }

}
