package org.sid.gmao_back.controller;


import org.sid.gmao_back.entities.CategorieVehicule;
import org.sid.gmao_back.entities.TypeAssurance;
import org.sid.gmao_back.repositories.CategorieVehiculeRepository;
import org.sid.gmao_back.service.CategorieVehiculeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Pageable;
import java.util.List;

@RestController
@RequestMapping(value = "/categorieVehicule")
@CrossOrigin("*")
public class CategorieVehiculeController {
    @Autowired
    private CategorieVehiculeService categorieVehiculeService;
    @Autowired
    private CategorieVehiculeRepository categorieVehiculeRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/list" )
    @ResponseBody
    public List<CategorieVehicule> findAll(){ return categorieVehiculeService.findAll();}

    @RequestMapping(method = RequestMethod.GET, value = "/list/{id}")
    public CategorieVehicule findById(@PathVariable(name = "id") Long id){ return categorieVehiculeService.findBydId(id);}

    @RequestMapping(method = RequestMethod.POST,consumes = {
            MediaType.APPLICATION_JSON_VALUE},produces = {MediaType.APPLICATION_JSON_VALUE}, value = "/list")

    public ResponseEntity save(@RequestBody CategorieVehicule categorieVehicule)
    {
        return new ResponseEntity(categorieVehiculeService.save(categorieVehicule),HttpStatus.OK );
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/list/{id}")
    public CategorieVehicule update(@PathVariable(name = "id") Long id, @RequestBody CategorieVehicule categorieVehicule)
    {
        return categorieVehiculeService.update(id,categorieVehicule);
    }

    @RequestMapping(method = RequestMethod.DELETE,value ="/list/{id}")
    public void delete(@PathVariable(name = "id") Long id){categorieVehiculeService.delete(id);}

    @RequestMapping(method = RequestMethod.GET,value = "/typeAssurance")
    public List<TypeAssurance> afficherTypeAssurance(){
        return (List<TypeAssurance>) categorieVehiculeService.typeAssurance();
    }
    @RequestMapping(method = RequestMethod.GET,value = "/searchBy")
    public Page<CategorieVehicule> showPage(@RequestParam(defaultValue = "0") int page){
        return categorieVehiculeRepository.findAll(PageRequest.of(page,5));
    }


}
