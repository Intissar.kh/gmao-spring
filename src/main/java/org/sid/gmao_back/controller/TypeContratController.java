package org.sid.gmao_back.controller;


import org.sid.gmao_back.entities.TermeAssurance;
import org.sid.gmao_back.entities.TypeAssurance;
import org.sid.gmao_back.entities.TypeContrat;
import org.sid.gmao_back.exceptions.IdNotFound;
import org.sid.gmao_back.service.TypeAssuranceService;
import org.sid.gmao_back.service.TypeContratService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/typeContrat")
public class TypeContratController {
    @Autowired
    private TypeContratService typeContratService;

    @RequestMapping(method = RequestMethod.GET ,value = "/list")
    @ResponseBody
    public List<TypeContrat> findAll()
    {
        return typeContratService.findAll();
    }

    @RequestMapping(method = RequestMethod.GET,value = "/list/{id}")
    @ResponseBody
    public TypeContrat findById(@PathVariable(name ="id") Long id) throws IdNotFound {
        return typeContratService.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET,value = "/list/{code}")
    @ResponseBody
    public TypeContrat findByCode(@PathVariable(name = "code") String code)
    {
        return typeContratService.findByCode(code);
    }
    @RequestMapping(method = RequestMethod.POST,value = "/list")
    @ResponseBody
    public TypeContrat save(@RequestBody() TypeContrat typeContrat)
    {
        return typeContratService.save(typeContrat);
    }
    @RequestMapping(method = RequestMethod.PUT,value = "/list/{id}")
    @ResponseBody
    public TypeContrat update(@PathVariable(name = "id")Long id ,@RequestBody TypeContrat typeContrat)
    {
        return typeContratService.update(id,typeContrat);
    }

    @RequestMapping(method = RequestMethod.DELETE,value = "/list/{id}")
    @ResponseBody
    public void delete(@PathVariable(name = "id") Long id)
    {
        typeContratService.delete(id);
    }
}
