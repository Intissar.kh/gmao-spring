package org.sid.gmao_back.controller;

import org.sid.gmao_back.entities.TermeAssurance;
import org.sid.gmao_back.entities.TypeAssurance;
import org.sid.gmao_back.exceptions.IdNotFound;
import org.sid.gmao_back.service.TypeAssuranceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/typeAssurance")
public class TypeAssuranceController {
    @Autowired
    private TypeAssuranceService typeAssuranceService;

    @RequestMapping(method = RequestMethod.GET ,value = "/list")
    @ResponseBody
    public List<TypeAssurance> findAll()
    {
        return typeAssuranceService.findAll();
    }

    @RequestMapping(method = RequestMethod.GET,value = "/list/{id}")
    @ResponseBody
    public TypeAssurance findById(@PathVariable(name ="id") Long id) throws IdNotFound {
        return typeAssuranceService.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET,value = "/list/{code}")
    @ResponseBody
    public TermeAssurance findByCode(@PathVariable(name = "code") String code)
    {
        return typeAssuranceService.findByCode(code);
    }
    @RequestMapping(method = RequestMethod.POST,value = "/list")
    @ResponseBody
    public TypeAssurance save(@RequestBody() TypeAssurance typeAssurance)
    {
        return typeAssuranceService.save(typeAssurance);
    }
    @RequestMapping(method = RequestMethod.PUT,value = "/list/{id}")
    @ResponseBody
    public TypeAssurance update(@PathVariable(name = "id")Long id ,@RequestBody TypeAssurance typeAssurance)
    {
        return typeAssuranceService.update(id,typeAssurance);
    }

    @RequestMapping(method = RequestMethod.DELETE,value = "/list/{id}")
    @ResponseBody
    public void delete(@PathVariable(name = "id") Long id)
    {
        typeAssuranceService.delete(id);
    }
}
