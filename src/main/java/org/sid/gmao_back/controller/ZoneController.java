package org.sid.gmao_back.controller;


import org.sid.gmao_back.entities.EtatMaintenance;
import org.sid.gmao_back.entities.TypePanne;
import org.sid.gmao_back.entities.Zone;
import org.sid.gmao_back.exceptions.IdNotFound;
import org.sid.gmao_back.service.EtatMaintenanceService;
import org.sid.gmao_back.service.ZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin("*")
@RestController
@RequestMapping(value= "/zone")
public class ZoneController {

    @Autowired
    private ZoneService zoneService;

    @RequestMapping(method = RequestMethod.GET,value = "/list")
    @ResponseBody
    public List<Zone> findAll(){
        return zoneService.findAll();
    }

    @RequestMapping(method = RequestMethod.GET,value = "/list/{id}")
    @ResponseBody
    public Zone findById(@PathVariable(name = "id") Long id) {
        return zoneService.findById(id);
    }
    @RequestMapping(method = RequestMethod.POST,value = "/list")
    @ResponseBody
    public Zone save(@RequestBody Zone zone){
        return zoneService.save(zone);
    }
    @RequestMapping(method = RequestMethod.PUT,value = "/list/{id}")
    @ResponseBody
    public Zone update(@PathVariable(name = "id") Long id,@RequestBody Zone zone){
        return zoneService.update(id,zone);
    }
    @RequestMapping(method = RequestMethod.DELETE,value = "/list/{id}")
    @ResponseBody
    public void delete(@PathVariable(name = "id") Long id){
        zoneService.delete(id);
    }


}
