package org.sid.gmao_back.controller;

import org.sid.gmao_back.entities.CategorieTransport;
import org.sid.gmao_back.entities.TypeMaintenance;
import org.sid.gmao_back.service.CategorieTransportService;
import org.sid.gmao_back.service.TypeMaintenanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/typeMaintenance")
public class TypeMaintenanceController {
    @Autowired
    private TypeMaintenanceService typeMaintenanceService;

    @RequestMapping(method = RequestMethod.GET, value = "/list" )
    @ResponseBody
    public List<TypeMaintenance> findAll(){ return typeMaintenanceService.findAll();}

    @RequestMapping(method = RequestMethod.GET, value = "/list/{id}")
    @ResponseBody
    public TypeMaintenance findById(@PathVariable(name = "id") Long id){ return typeMaintenanceService.findById(id);}

    @RequestMapping(method = RequestMethod.POST, value = "/list")
    @ResponseBody
    public TypeMaintenance save(@RequestBody TypeMaintenance typeMaintenance)
    {
        return typeMaintenanceService.save(typeMaintenance);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/list/{id}")
    @ResponseBody
    public TypeMaintenance update(@PathVariable(name = "id") Long id, @RequestBody TypeMaintenance typeMaintenance)
    {
        return typeMaintenanceService.update(id,typeMaintenance);
    }

    @RequestMapping(method = RequestMethod.DELETE,value ="/list/{id}")
    @ResponseBody
    public void delete(@PathVariable(name = "id") Long id){typeMaintenanceService.delete(id);}

}
