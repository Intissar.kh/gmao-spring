package org.sid.gmao_back.controller;

import org.sid.gmao_back.entities.CategorieVehicule;
import org.sid.gmao_back.entities.Chauffeur;
import org.sid.gmao_back.entities.TypeAssurance;
import org.sid.gmao_back.service.ChauffeurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping(value = "/chauffeur")
@CrossOrigin("*")

public class ChauffeurController {

    @Autowired
    private ChauffeurService chauffeurService;
    @RequestMapping(method = RequestMethod.GET, value = "/list" )
    @ResponseBody
    public List<Chauffeur> findAll(){ return chauffeurService.findAll();}

    @RequestMapping(method = RequestMethod.GET, value = "/list/{id}")
    public Chauffeur findById(@PathVariable(name = "id") Long id){ return chauffeurService.findById(id);}

    @RequestMapping(method = RequestMethod.POST,consumes = {
            MediaType.APPLICATION_JSON_VALUE},produces = {MediaType.APPLICATION_JSON_VALUE}, value = "/list")

    public ResponseEntity save(@RequestBody Chauffeur chauffeur)
    {
        return new ResponseEntity(chauffeurService.save(chauffeur), HttpStatus.OK );
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/list/{id}")
    public Chauffeur update(@PathVariable(name = "id") Long id, @RequestBody Chauffeur chauffeur)
    {
        return chauffeurService.update(id,chauffeur);
    }

    @RequestMapping(method = RequestMethod.DELETE,value ="/list/{id}")
    public void delete(@PathVariable(name = "id") Long id){chauffeurService.delete(id);}


}
