package org.sid.gmao_back.controller;

import org.sid.gmao_back.entities.TypeContrat;
import org.sid.gmao_back.entities.TypeEau;
import org.sid.gmao_back.exceptions.IdNotFound;
import org.sid.gmao_back.service.TypeContratService;
import org.sid.gmao_back.service.TypeEauService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value ="/typeEau")
public class TypeEauController {
    @Autowired
    private TypeEauService typeEauService;

    @RequestMapping(method = RequestMethod.GET ,value = "/list")
    @ResponseBody
    public List<TypeEau> findAll()
    {
        return typeEauService.findAll();
    }

    @RequestMapping(method = RequestMethod.GET,value = "/list/{id}")
    @ResponseBody
    public TypeEau findById(@PathVariable(name ="id") Long id) throws IdNotFound {
        return typeEauService.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET,value = "/list/{code}")
    @ResponseBody
    public TypeEau findByCode(@PathVariable(name = "code") String code)
    {
        return typeEauService.findByCode(code);
    }
    @RequestMapping(method = RequestMethod.POST,value = "/list")
    @ResponseBody
    public TypeEau save(@RequestBody() TypeEau typeEau)
    {
        return typeEauService.save(typeEau);
    }
    @RequestMapping(method = RequestMethod.PUT,value = "/list/{id}")
    @ResponseBody
    public TypeEau update(@PathVariable(name = "id")Long id ,@RequestBody TypeEau typeEau)
    {
        return typeEauService.update(id,typeEau);
    }

    @RequestMapping(method = RequestMethod.DELETE,value = "/list/{id}")
    @ResponseBody
    public void delete(@PathVariable(name = "id") Long id)
    {
        typeEauService.delete(id);
    }
}
