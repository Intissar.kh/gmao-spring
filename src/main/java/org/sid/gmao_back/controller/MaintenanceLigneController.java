package org.sid.gmao_back.controller;

import org.sid.gmao_back.entities.EtatMaintenance;
import org.sid.gmao_back.entities.MaintenanceLigne;
import org.sid.gmao_back.exceptions.IdNotFound;
import org.sid.gmao_back.service.EtatMaintenanceService;
import org.sid.gmao_back.service.MaintenanceLigneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/maintenanceLigne")
public class MaintenanceLigneController {
    @Autowired
    private MaintenanceLigneService maintenanceLigneService;

    @RequestMapping(method = RequestMethod.GET,value = "/list")
    @ResponseBody
    public List<MaintenanceLigne> findAll(){
        return maintenanceLigneService.findAll();
    }

    @RequestMapping(method = RequestMethod.GET,value = "/list/{id}")
    @ResponseBody
    public MaintenanceLigne findById(@PathVariable(name = "id") Long id) throws IdNotFound {
        return maintenanceLigneService.findById(id);
    }
    @RequestMapping(method = RequestMethod.POST,value = "/list")
    @ResponseBody
    public MaintenanceLigne save(@RequestBody MaintenanceLigne maintenanceLigne){
        return maintenanceLigneService.save(maintenanceLigne);
    }
    @RequestMapping(method = RequestMethod.PUT,value = "/list/{id}")
    @ResponseBody
    public MaintenanceLigne update(@PathVariable(name = "id") Long id,@RequestBody MaintenanceLigne maintenanceLigne){
        return maintenanceLigneService.update(id,maintenanceLigne);
    }
    @RequestMapping(method = RequestMethod.DELETE,value = "/list/{id}")
    @ResponseBody
    public void delete(@PathVariable(name = "id") Long id){
        maintenanceLigneService.delete(id);
    }

}
