package org.sid.gmao_back.controller;

import org.sid.gmao_back.entities.Task;
import org.sid.gmao_back.repositories.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/tasks")
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;

    //@RequestMapping(method = RequestMethod.GET,value = "/tasks")
    //@ResponseBody
    @GetMapping("/tasks")
    private List<Task> listTask(){
        return taskRepository.findAll();
    }
    @PostMapping("/tasks")
    //@RequestMapping(method = RequestMethod.POST,value = "/tasks")
    //@ResponseBody
    private Task save(@RequestBody Task task){
        return taskRepository.save(task);
    }

}
