package org.sid.gmao_back.controller;

import org.sid.gmao_back.entities.TermeAssurance;
import org.sid.gmao_back.entities.TypeAssurance;
import org.sid.gmao_back.entities.TypeBadge;
import org.sid.gmao_back.exceptions.IdNotFound;
import org.sid.gmao_back.service.TypeAssuranceService;
import org.sid.gmao_back.service.TypeBadgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/typeBadge")
public class TypeBadgeController {
    @Autowired
    private TypeBadgeService typeBadgeService;

    @RequestMapping(method = RequestMethod.GET ,value = "/list")
    @ResponseBody
    public List<TypeBadge> findAll()
    {
        return typeBadgeService.findAll();
    }

    @RequestMapping(method = RequestMethod.GET,value = "/list/{id}")
    @ResponseBody
    public TypeBadge findById(@PathVariable(name ="id") Long id) throws IdNotFound {
        return typeBadgeService.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET,value = "/list/{code}")
    @ResponseBody
    public TypeBadge findByCode(@PathVariable(name = "code") String code)
    {
        return typeBadgeService.findByCode(code);
    }
    @RequestMapping(method = RequestMethod.POST,value = "/list")
    @ResponseBody
    public TypeBadge save(@RequestBody() TypeBadge typeBadge)
    {
        return typeBadgeService.save(typeBadge);
    }
    @RequestMapping(method = RequestMethod.PUT,value = "/list/{id}")
    @ResponseBody
    public TypeBadge update(@PathVariable(name = "id")Long id ,@RequestBody TypeBadge typeBadge)
    {
        return typeBadgeService.update(id,typeBadge);
    }

    @RequestMapping(method = RequestMethod.DELETE,value = "/list/{id}")
    @ResponseBody
    public void delete(@PathVariable(name = "id") Long id) {
        typeBadgeService.delete(id);
    }
}
