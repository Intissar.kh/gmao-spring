package org.sid.gmao_back.controller;


import org.sid.gmao_back.entities.TypeContrat;
import org.sid.gmao_back.entities.TypeHuile;
import org.sid.gmao_back.exceptions.IdNotFound;
import org.sid.gmao_back.service.TypeHuileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/typeHuile")
public class TypeHuileCotroller {

    @Autowired
    private TypeHuileService typeHuileService;

    @RequestMapping(method = RequestMethod.GET ,value = "/list")
    @ResponseBody
    public List<TypeHuile> findAll()
    {
        return typeHuileService.findAll();
    }

    @RequestMapping(method = RequestMethod.GET,value = "/list/{id}")
    @ResponseBody
    public TypeHuile findById(@PathVariable(name ="id") Long id) throws IdNotFound {
        return typeHuileService.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET,value = "/list/{code}")
    @ResponseBody
    public TypeHuile findByCode(@PathVariable(name = "code") String code)
    {
        return typeHuileService.findByCode(code);
    }
    @RequestMapping(method = RequestMethod.POST,value = "/list")
    @ResponseBody
    public TypeHuile save(@RequestBody() TypeHuile typeHuile)
    {
        return typeHuileService.save(typeHuile);
    }
    @RequestMapping(method = RequestMethod.PUT,value = "/list/{id}")
    @ResponseBody
    public TypeHuile update(@PathVariable(name = "id")Long id ,@RequestBody TypeHuile typeHuile)
    {
        return typeHuileService.update(id,typeHuile);
    }

    @RequestMapping(method = RequestMethod.DELETE,value = "/list/{id}")
    @ResponseBody
    public void delete(@PathVariable(name = "id") Long id)
    {
        typeHuileService.delete(id);
    }
}
