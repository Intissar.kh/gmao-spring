package org.sid.gmao_back.controller;

import org.sid.gmao_back.entities.*;
import org.sid.gmao_back.service.MaintenanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/maintenance")
@CrossOrigin("*")
public class MaintenanceController {
    @Autowired
    private MaintenanceService maintenanceService;
    @RequestMapping(method = RequestMethod.GET, value = "/list" )
    @ResponseBody
    public List<Maintenance> findAll(){ return maintenanceService.findAll();}

    @RequestMapping(method = RequestMethod.GET, value = "/list/{id}")
    public Maintenance findById(@PathVariable(name = "id") Long id){ return maintenanceService.findById(id);}

    @RequestMapping(method = RequestMethod.POST,consumes = {
            MediaType.APPLICATION_JSON_VALUE},produces = {MediaType.APPLICATION_JSON_VALUE}, value = "/list")

    public ResponseEntity save(@RequestBody Maintenance maintenance)
    {
        return new ResponseEntity(maintenanceService.save(maintenance), HttpStatus.OK );
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/list/{id}")
    public Maintenance update(@PathVariable(name = "id") Long id, @RequestBody Maintenance maintenance)
    {
        return maintenanceService.update(id,maintenance);
    }

    @RequestMapping(method = RequestMethod.DELETE,value ="/list/{id}")
    public void delete(@PathVariable(name = "id") Long id){maintenanceService.delete(id);}


    @RequestMapping(method = RequestMethod.GET,value = "/vehicule")
    public List<Vehicule> afficherVehicule(){
        return (List<Vehicule>) maintenanceService.vehicule();
    }
    @RequestMapping(method = RequestMethod.GET,value = "/typePanne")
    public List<TypePanne> afficherTypePanne(){
        return (List<TypePanne>) maintenanceService.typePanne();
    }
    @RequestMapping(method = RequestMethod.GET,value = "/typePiece")
    public List<Piece> afficherTypePiece(){
        return (List<Piece>) maintenanceService.typePiece();
    }
    @RequestMapping(method = RequestMethod.GET,value = "/typeHuile")
    public List<TypeHuile> afficherTypeHuile(){
        return (List<TypeHuile>) maintenanceService.typeHuile();
    }
    @RequestMapping(method = RequestMethod.GET,value = "/typeEau")
    public List<TypeEau> afficherTypeEau(){
        return (List<TypeEau>) maintenanceService.typeEau();
    }
    @RequestMapping(method = RequestMethod.GET,value = "/etatMaintenance")
    public List<EtatMaintenance> afficherEtatMaintenance(){
        return (List<EtatMaintenance>) maintenanceService.etatMaintenance();
    }
    @RequestMapping(method = RequestMethod.GET,value = "/typeMaintenance")
    public List<TypeMaintenance> afficherTypeMaintenance(){
        return (List<TypeMaintenance>) maintenanceService.typeMaintenance();
    }

}
