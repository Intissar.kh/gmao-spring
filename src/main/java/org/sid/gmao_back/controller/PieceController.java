package org.sid.gmao_back.controller;

import org.sid.gmao_back.entities.Piece;
import org.sid.gmao_back.entities.TermeAssurance;
import org.sid.gmao_back.service.PieceService;
import org.sid.gmao_back.service.TermeAssuranceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping(value = "/piece")
public class PieceController {
    @Autowired
    private PieceService pieceService;

    @RequestMapping(method = RequestMethod.GET ,value = "/list")
    @ResponseBody
    public List<Piece> findAll()
    {
        return pieceService.findAll();
    }

    @RequestMapping(method = RequestMethod.GET,value = "/list/{id}")
    @ResponseBody
    public Piece findById(@PathVariable(name ="id") Long id)
    {
        return pieceService.findById(id);
    }

    @RequestMapping(method = RequestMethod.POST,value = "/list")
    @ResponseBody
    public Piece save(@RequestBody Piece piece)
    {
        return pieceService.save(piece);
    }
    @RequestMapping(method = RequestMethod.PUT,value = "/list/{id}")
    @ResponseBody
    public Piece update(@PathVariable(name = "id")Long id ,@RequestBody Piece piece)
    {
        return pieceService.update(id,piece);
    }

    @RequestMapping(method = RequestMethod.DELETE,value = "/list/{id}")
    @ResponseBody
    public void delete(@PathVariable(name = "id") Long id)
    {
        pieceService.delete(id);
    }
}
