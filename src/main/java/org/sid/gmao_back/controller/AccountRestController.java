package org.sid.gmao_back.controller;

import org.sid.gmao_back.entities.AppUser;
import org.sid.gmao_back.repositories.AppUserRepository;
import org.sid.gmao_back.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value="/user")
public class AccountRestController {

   @Autowired
   private AccountService accountService;
   @Autowired
   private AppUserRepository appUserRepository;
    @Qualifier("userDetailsServiceImpl")
    @Autowired
   private UserDetailsService service;

    @RequestMapping(method = RequestMethod.GET,value = "/list")
    @ResponseBody
    public List<AppUser> findAll(){
        return accountService.findAll();
    }

    @RequestMapping(method = RequestMethod.GET,value = "/list/{id}")
    @ResponseBody
    public AppUser findById(@PathVariable(name = "id") Long id)  {
        return accountService.findById(id);
    }
    @RequestMapping(value = "/username", method = RequestMethod.GET)
    @ResponseBody
    public void currentUserName() {
      /*  Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            String username = ((UserDetails)principal).getUsername();
            System.out.println(username);
        } else {
            String username = principal.toString();
            System.out.println("not found"+username);
        }*/
    }
    @RequestMapping(method = RequestMethod.GET,value = "/password")
    @ResponseBody
    public String findPassword(@PathVariable(name = "password") String password)  {
        System.out.println(accountService.findPassword(password));
        return accountService.findPassword(password);
    }
    @RequestMapping(method = RequestMethod.PUT,value = "/list/{id}")
    @ResponseBody
    public AppUser update(@PathVariable(name = "id") Long id,@RequestBody AppUser user){
        return accountService.update(id,user);
    }

}
