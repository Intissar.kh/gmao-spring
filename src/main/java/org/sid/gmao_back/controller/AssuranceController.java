package org.sid.gmao_back.controller;

import org.sid.gmao_back.entities.Assurance;
import org.sid.gmao_back.entities.EtatMaintenance;
import org.sid.gmao_back.exceptions.IdNotFound;
import org.sid.gmao_back.service.AssuranceService;
import org.sid.gmao_back.service.EtatMaintenanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/assurance")
public class AssuranceController {
    @Autowired
    private AssuranceService assuranceService;

    @RequestMapping(method = RequestMethod.GET,value = "/list")
    @ResponseBody
    public List<Assurance> findAll(){
        return assuranceService.findAll();
    }

    @RequestMapping(method = RequestMethod.GET,value = "/list/{id}")
    @ResponseBody
    public Assurance findById(@PathVariable(name = "id") Long id) throws IdNotFound {
        return assuranceService.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET,value = "/list/{code}")
    @ResponseBody
    public Assurance findByCode(@PathVariable(name = "code") String code) {
        return assuranceService.findByCode(code);
    }
    @RequestMapping(method = RequestMethod.POST,value = "/list")
    @ResponseBody
    public Assurance save(@RequestBody Assurance assurance){
        return assuranceService.save(assurance);
    }
    @RequestMapping(method = RequestMethod.PUT,value = "/list/{id}")
    @ResponseBody
    public Assurance update(@PathVariable(name = "id") Long id,@RequestBody Assurance assurance){
        return assuranceService.update(id,assurance);
    }
    @RequestMapping(method = RequestMethod.DELETE,value = "/list/{id}")
    @ResponseBody
    public void delete(@PathVariable(name = "id") Long id){
        assuranceService.delete(id);
    }


}
