package org.sid.gmao_back.controller;


import org.sid.gmao_back.entities.TermeAssurance;
import org.sid.gmao_back.service.TermeAssuranceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/termeAssurance")
public class TermeAssuranceControllor {

    @Autowired
    private TermeAssuranceService termeAssuranceService;

    @RequestMapping(method = RequestMethod.GET ,value = "/list")
    @ResponseBody
    public List<TermeAssurance> findAll()
    {
        return termeAssuranceService.findAll();
    }

    @RequestMapping(method = RequestMethod.GET,value = "/list/{id}")
    @ResponseBody
    public TermeAssurance findById(@PathVariable(name ="id") Long id)
    {
        return termeAssuranceService.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET,value = "/list/{code}")
    @ResponseBody
    public TermeAssurance findByCode(@PathVariable(name = "code") String code)
    {
        return termeAssuranceService.findByCode(code);
    }
    @RequestMapping(method = RequestMethod.POST,value = "/list")
    @ResponseBody
    public TermeAssurance save(@RequestBody() TermeAssurance termeAssurance)
    {
        return termeAssuranceService.save(termeAssurance);
    }
    @RequestMapping(method = RequestMethod.PUT,value = "/list/{id}")
    @ResponseBody
    public TermeAssurance update(@PathVariable(name = "id")Long id ,@RequestBody TermeAssurance termeAssurance)
    {
        return termeAssuranceService.update(id,termeAssurance);
    }

    @RequestMapping(method = RequestMethod.DELETE,value = "/list/{id}")
    @ResponseBody
    public void delete(@PathVariable(name = "id") Long id)
    {
        termeAssuranceService.delete(id);
    }
}
