package org.sid.gmao_back.controller;


import org.sid.gmao_back.entities.CatalogueTransport;
import org.sid.gmao_back.entities.CategorieTransport;
import org.sid.gmao_back.service.CatalogueTransportService;
import org.sid.gmao_back.service.CategorieTransportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/catalogueTransport")
public class CatalogueTransportControllor {
        @Autowired
        private CatalogueTransportService catalogueTransportService;

        @RequestMapping(method = RequestMethod.GET, value = "/list" )
        @ResponseBody
        public List<CatalogueTransport> findAll(){ return catalogueTransportService.findAll();}

        @RequestMapping(method = RequestMethod.GET, value = "/list/{id}")
        @ResponseBody
        public CatalogueTransport findById(@PathVariable(name = "id") Long id){ return catalogueTransportService.findById(id);}

        @RequestMapping(method = RequestMethod.POST, value = "/list")
        @ResponseBody
        public CatalogueTransport save(@RequestBody CatalogueTransport catalogueTransport)
        {
            return catalogueTransportService.save(catalogueTransport);
        }

        @RequestMapping(method = RequestMethod.PUT, value = "/list/{id}")
        @ResponseBody
        public CatalogueTransport update(@PathVariable(name = "id") Long id, @RequestBody CatalogueTransport catalogueTransport)
        {
            return catalogueTransportService.update(id,catalogueTransport);
        }

        @RequestMapping(method = RequestMethod.DELETE,value ="/list/{id}")
        @ResponseBody
        public void delete(@PathVariable(name = "id") Long id){catalogueTransportService.delete(id);}

    }