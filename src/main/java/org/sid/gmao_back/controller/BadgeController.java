package org.sid.gmao_back.controller;


import org.sid.gmao_back.entities.Badge;
import org.sid.gmao_back.entities.CatalogueTransport;
import org.sid.gmao_back.service.BadgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/badge")
public class BadgeController {
    @Autowired
    private BadgeService badgeService;

    @RequestMapping(method = RequestMethod.GET,value = "/list")
    @ResponseBody
    public List<Badge> findAll(){ return badgeService.findAll();}

    @RequestMapping(method = RequestMethod.GET ,value = "/list/{id}")
    @ResponseBody
    public Badge findById(@PathVariable(name = "id") Long id)
    {
        return badgeService.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET ,value = "/list/{code}")
    @ResponseBody
    public Badge findByCode(@PathVariable(name = "code") String code)
    {
        return badgeService.findByCode(code);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/list")
    @ResponseBody
    public Badge save(@RequestBody Badge badge)
    {
        return badgeService.save(badge);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/list/{id}")
    @ResponseBody
    public Badge update(@PathVariable(name = "id") Long id, @RequestBody Badge badge)
    {
        return badgeService.update(id,badge);
    }

    @RequestMapping(method = RequestMethod.DELETE,value ="/list/{id}")
    @ResponseBody
    public void delete(@PathVariable(name = "id") Long id){badgeService.delete(id);}



}
