package org.sid.gmao_back.controller;

import org.sid.gmao_back.entities.*;
import org.sid.gmao_back.repositories.CategorieVehiculeRepository;
import org.sid.gmao_back.repositories.VehiculeRepository;
import org.sid.gmao_back.service.CategorieVehiculeService;
import org.sid.gmao_back.service.VehiculeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping(value = "/vehicule")
@CrossOrigin("*")
public class VehiculeController {
    @Autowired
    private VehiculeService vehiculeService;
    @Autowired
    private VehiculeRepository vehiculeRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/list" )
    @ResponseBody
    public List<Vehicule> findAll(){ return vehiculeService.findAll();}

    @RequestMapping(method = RequestMethod.GET, value = "/list/{id}")
    public Vehicule findById(@PathVariable(name = "id") Long id){ return vehiculeService.findBydId(id);}

    @RequestMapping(method = RequestMethod.POST,consumes = {
            MediaType.APPLICATION_JSON_VALUE},produces = {MediaType.APPLICATION_JSON_VALUE}, value = "/list")

    public ResponseEntity save(@RequestBody Vehicule vehicule)
    {
        return new ResponseEntity(vehiculeService.save(vehicule), HttpStatus.OK );
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/list/{id}")
    public Vehicule update(@PathVariable(name = "id") Long id, @RequestBody Vehicule vehicule)
    {
        return vehiculeService.update(id,vehicule);
    }

    @RequestMapping(method = RequestMethod.DELETE,value ="/list/{id}")
    public void delete(@PathVariable(name = "id") Long id){vehiculeService.delete(id);}

    @RequestMapping(method = RequestMethod.GET,value = "/typeAssurance")
    public List<TypeAssurance> afficherTypeAssurance(){
        return (List<TypeAssurance>) vehiculeService.typeAssurance();
    }
    @RequestMapping(method = RequestMethod.GET,value = "/typeBadge")
    public List<TypeBadge> afficherBadge(){
        return (List<TypeBadge>) vehiculeService.typeBadge();
    }
    @RequestMapping(method = RequestMethod.GET,value = "/typeContrat")
    public List<TypeContrat> afficherTypeContrat(){
        return (List<TypeContrat>) vehiculeService.typeContrat();
    }
    @RequestMapping(method = RequestMethod.GET,value = "/zone")
    public List<Zone> afficherZone(){
        return (List<Zone>) vehiculeService.zone();
    }
    @RequestMapping(method = RequestMethod.GET,value = "/categorieVehicule")
    public List<CategorieVehicule> afficherCategorie(){
        return (List<CategorieVehicule>) vehiculeService.categorieVehicule();
    }
    @RequestMapping(method = RequestMethod.GET,value = "/searchBy")
    public Page<Vehicule> showPage(@RequestParam(defaultValue = "0") int page){
        return vehiculeRepository.findAll(PageRequest.of(page,5));
    }


}
