package org.sid.gmao_back.controller;

import org.sid.gmao_back.entities.EtatMaintenance;
import org.sid.gmao_back.exceptions.IdNotFound;
import org.sid.gmao_back.service.EtatMaintenanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin("*")
@RequestMapping(value="/etatMaintenance")
public class EtatMaintenanceController {
    @Autowired
    private EtatMaintenanceService etatMaintenanceService;

    @RequestMapping(method = RequestMethod.GET,value = "/list")
    @ResponseBody
    public List<EtatMaintenance> findAll(){
        return etatMaintenanceService.findAll();
    }

    //@GetMapping(value = "/list/{id}")
    @RequestMapping(method = RequestMethod.GET,value = "/list/{id}")
    @ResponseBody
    public EtatMaintenance findById(@PathVariable(name = "id") Long id) {
        return etatMaintenanceService.findById(id);
    }
    // @PostMapping(value = "/list")
    @RequestMapping(method = RequestMethod.POST,value = "/list")
    @ResponseBody
    public EtatMaintenance save(@RequestBody EtatMaintenance etatMaintenance){
        return etatMaintenanceService.save(etatMaintenance);
    }
    //@PutMapping(value = "/list/{id}")
    @RequestMapping(method = RequestMethod.PUT,value = "/list/{id}")
    @ResponseBody
    public EtatMaintenance update(@PathVariable(name = "id") Long id,@RequestBody EtatMaintenance etatMaintenance){
        return etatMaintenanceService.update(id,etatMaintenance);
    }
    @RequestMapping(method = RequestMethod.DELETE,value = "/list/{id}")
    @ResponseBody
    public void delete(@PathVariable(name = "id") Long id){
        etatMaintenanceService.delete(id);
    }


}
