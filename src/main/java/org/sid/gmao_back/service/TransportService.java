package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.EtatMaintenance;
import org.sid.gmao_back.entities.Transport;
import org.sid.gmao_back.exceptions.IdNotFound;

import java.util.List;

public interface TransportService {
    List<Transport> findAll();
    Transport findById(Long id) throws IdNotFound;
    Transport findByCode(String code) ;
    Transport update(Long id,Transport transport);
    Transport save(Transport transport);
    void delete(long id);
}
