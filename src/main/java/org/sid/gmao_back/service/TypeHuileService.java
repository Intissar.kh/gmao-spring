package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.TypeContrat;
import org.sid.gmao_back.entities.TypeHuile;

import java.util.List;

public interface TypeHuileService {
    List<TypeHuile> findAll();
    TypeHuile findById(Long id);
    TypeHuile save (TypeHuile typeHuile);
    TypeHuile update(Long id , TypeHuile typeHuile);
    void delete(Long id);
    TypeHuile findByCode(String code);
}
