package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.TypeBadge;

import java.util.List;

public interface TypeBadgeService {
    List<TypeBadge> findAll();
    TypeBadge findById(Long id);
    TypeBadge save (TypeBadge typeBadge);
    TypeBadge update(Long id , TypeBadge typeBadge);
    void delete(Long id);
    TypeBadge findByCode(String code);
}
