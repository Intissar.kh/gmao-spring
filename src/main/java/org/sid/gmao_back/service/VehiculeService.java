package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.*;

import java.util.List;

public interface VehiculeService {
    List<Vehicule> findAll();
    Vehicule findBydId(Long id);
    Vehicule save (Vehicule vehicule);
    Vehicule update(Long id , Vehicule vehicule);
    void delete(Long id);
    List<TypeAssurance> typeAssurance();
    List<CategorieVehicule> categorieVehicule();
    List<TypeBadge> typeBadge();
    List<TypeContrat> typeContrat();
    List<Zone> zone();
    Vehicule findByCode(String code);
}
