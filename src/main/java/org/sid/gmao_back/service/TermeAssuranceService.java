package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.TermeAssurance;

import java.util.List;

public interface TermeAssuranceService {

    List<TermeAssurance> findAll();
    TermeAssurance findById(Long id);
    TermeAssurance findByCode(String code);
    TermeAssurance save(TermeAssurance termeAssurance);
    TermeAssurance update(Long id, TermeAssurance termeAssurance);
    void delete(Long id);
}
