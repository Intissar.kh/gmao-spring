package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.Badge;
import org.sid.gmao_back.entities.CategorieVehicule;

import java.util.List;

public interface BadgeService {
    List<Badge> findAll();
    Badge findById(Long id);
    Badge save (Badge badge);
    Badge update(Long id , Badge badge);
    void delete(Long id);
    Badge findByCode(String code);
}
