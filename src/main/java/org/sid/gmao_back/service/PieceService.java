package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.Piece;
import org.sid.gmao_back.entities.TermeAssurance;

import java.util.List;

public interface PieceService {
    List<Piece> findAll();
    Piece findById(Long id);
    Piece save(Piece piece);
    Piece update(Long id, Piece piece);
    void delete(Long id);
}
