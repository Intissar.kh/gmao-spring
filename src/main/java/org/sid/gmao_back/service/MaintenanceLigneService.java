package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.EtatMaintenance;
import org.sid.gmao_back.entities.MaintenanceLigne;
import org.sid.gmao_back.exceptions.IdNotFound;

import java.util.List;

public interface MaintenanceLigneService {
    List<MaintenanceLigne> findAll();
    MaintenanceLigne findById(Long id) throws IdNotFound;
    MaintenanceLigne update(Long id,MaintenanceLigne maintenanceLigne);
    MaintenanceLigne save(MaintenanceLigne maintenanceLigne);
    void delete(long id);
}
