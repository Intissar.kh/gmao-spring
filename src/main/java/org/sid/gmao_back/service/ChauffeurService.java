package org.sid.gmao_back.service;
import org.sid.gmao_back.entities.Chauffeur;

import java.util.List;

public interface ChauffeurService {
    List<Chauffeur> findAll();
    Chauffeur findById(Long id);
    Chauffeur save (Chauffeur chauffeur);
    Chauffeur update(Long id , Chauffeur chauffeur);
    void delete(Long id);
    Chauffeur findByCode(String code);
}
