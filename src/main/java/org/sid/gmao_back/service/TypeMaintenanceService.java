package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.EtatMaintenance;
import org.sid.gmao_back.entities.TypeMaintenance;

import java.lang.reflect.Type;
import java.util.List;

public interface TypeMaintenanceService {

    List<TypeMaintenance> findAll();
    TypeMaintenance findById(Long id);
    TypeMaintenance save(TypeMaintenance typeMaintenance);
    TypeMaintenance update(Long id,TypeMaintenance typeMaintenance);
    void delete(Long id);
}
