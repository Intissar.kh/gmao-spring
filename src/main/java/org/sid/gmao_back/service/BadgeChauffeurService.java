package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.*;

import java.util.List;

public interface BadgeChauffeurService {
    List<BadgeChauffeur> findAll();
    BadgeChauffeur findBydId(Long id);
    BadgeChauffeur save (BadgeChauffeur badgeChauffeur);
    BadgeChauffeur update(Long id , BadgeChauffeur badgeChauffeur);
    void delete(Long id);
    List<Chauffeur> chauffeur();
    List<TypeBadge> typeBadge();
}
