package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.CatalogueTransport;

import java.util.List;

public interface CatalogueTransportService {
    List<CatalogueTransport> findAll();
    CatalogueTransport findById(Long id);
    CatalogueTransport save (CatalogueTransport catalogueTransport);
    CatalogueTransport update(Long id , CatalogueTransport catalogueTransport);
    void delete(Long id);
}
