package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.Badge;
import org.sid.gmao_back.entities.CategorieVehicule;
import org.sid.gmao_back.entities.TypeAssurance;

import java.util.List;

public interface CategorieVehiculeService {

     List<CategorieVehicule> findAll();
     CategorieVehicule findBydId(Long id);
     CategorieVehicule save (CategorieVehicule categorieVehicule);
     CategorieVehicule update(Long id , CategorieVehicule gmaoVehiculeCategory);
     void delete(Long id);
     List<TypeAssurance> typeAssurance();
    CategorieVehicule findByCode(String code);

}
