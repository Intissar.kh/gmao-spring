package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.*;

import java.util.List;

public interface MaintenanceService {
    List<Maintenance> findAll();
    Maintenance findById(Long id);
    Maintenance save (Maintenance maintenance);
    Maintenance update(Long id , Maintenance maintenance);
    void delete(Long id);
    List<Vehicule> vehicule();
    List<TypePanne> typePanne();
    List<Piece> typePiece();
    List<TypeHuile> typeHuile();
    List<TypeEau> typeEau();
    List<EtatMaintenance> etatMaintenance();
    List<TypeMaintenance> typeMaintenance();

}
