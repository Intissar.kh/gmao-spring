package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.TypeMaintenance;
import org.sid.gmao_back.entities.TypePanne;

import java.util.List;

public interface TypePanneService {
    List<TypePanne> findAll();
    TypePanne findById(Long id);
    TypePanne save(TypePanne typePanne);
    TypePanne update(Long id,TypePanne typePanne);
    void delete(Long id);
}
