package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.TermeAssurance;
import org.sid.gmao_back.entities.TypeAssurance;
import org.sid.gmao_back.exceptions.IdNotFound;

import java.util.List;

public interface TypeAssuranceService {
    List<TypeAssurance> findAll();
    TypeAssurance findById(Long id) throws IdNotFound;
    TermeAssurance findByCode(String code);
    TypeAssurance save(TypeAssurance typeAssurance);
    TypeAssurance update(Long id, TypeAssurance typeAssurance);
    void delete(Long id);
}
