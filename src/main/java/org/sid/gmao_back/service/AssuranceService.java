package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.Assurance;
import org.sid.gmao_back.entities.EtatMaintenance;
import org.sid.gmao_back.exceptions.IdNotFound;

import java.util.List;

public interface AssuranceService {
    List<Assurance> findAll();
    Assurance findById(Long id) throws IdNotFound;
    Assurance findByCode(String code) ;
    Assurance update(Long id,Assurance assurance);
    Assurance save(Assurance assurance);
    void delete(long id);
}
