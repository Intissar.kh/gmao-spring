package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.TypeContrat;
import org.sid.gmao_back.entities.TypeEau;

import java.util.List;

public interface TypeEauService {
    List<TypeEau> findAll();
    TypeEau findById(Long id);
    TypeEau save (TypeEau typeEau);
    TypeEau update(Long id , TypeEau typeEau);
    void delete(Long id);
    TypeEau findByCode(String code);
}
