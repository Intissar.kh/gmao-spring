package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.TypePanne;
import org.sid.gmao_back.entities.Zone;
import org.sid.gmao_back.exceptions.IdNotFound;

import java.util.List;

public interface ZoneService {
    List<Zone> findAll();
    Zone findById(Long id);
    Zone save(Zone zone);
    Zone update(Long id,Zone zone);
    void delete(Long id);
}
