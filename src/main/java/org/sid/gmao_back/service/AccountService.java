package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.AppRole;
import org.sid.gmao_back.entities.AppUser;
import org.sid.gmao_back.entities.Assurance;
import org.sid.gmao_back.exceptions.IdNotFound;

import java.util.List;

public interface AccountService {

    public AppUser saveUser(AppUser user);
    List<AppUser> findAll();
    AppUser findById(Long id) ;
    String findUsername(String user);
    String findPassword(String password);
    AppUser update(Long id,AppUser user);
    public AppRole saveRole(AppRole role);
    public void addRoleToUser(String username,String role);
    public AppUser findUserByUsername(String username);
}
