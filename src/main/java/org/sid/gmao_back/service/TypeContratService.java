package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.TypeAssurance;
import org.sid.gmao_back.entities.TypeContrat;

import java.util.List;

public interface TypeContratService {
    List<TypeContrat> findAll();
    TypeContrat findById(Long id);
    TypeContrat save (TypeContrat typeContrat);
    TypeContrat update(Long id , TypeContrat typeContrat);
    void delete(Long id);
    TypeContrat findByCode(String code);

}
