package org.sid.gmao_back.service;

import org.sid.gmao_back.entities.CategorieTransport;
import org.sid.gmao_back.entities.CategorieVehicule;

import java.util.List;

public interface CategorieTransportService {
    List<CategorieTransport> findAll();
    CategorieTransport findById(Long id);
    CategorieTransport save (CategorieTransport categorieTransport);
    CategorieTransport update(Long id , CategorieTransport CategorieTransport);
    void delete(Long id);
}
