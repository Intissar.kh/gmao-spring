package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.Badge;
import org.sid.gmao_back.repositories.BadgeRepository;
import org.sid.gmao_back.service.BadgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BadgeServiceImpl implements BadgeService {

    @Autowired
    private BadgeRepository badgeRepository;
    @Override
    public List<Badge> findAll() {
        return badgeRepository.findAll();
    }

    @Override
    public Badge findById(Long id) {
        return badgeRepository.findById(id).get();
    }

    @Override
    public Badge save(Badge badge) {
        return badgeRepository.save(badge);
    }

    @Override
    public Badge update(Long id, Badge badge) {
        badge.setId(id);
        return badgeRepository.save(badge);
    }

    @Override
    public void delete(Long id) {
        badgeRepository.deleteById(id);
    }

    @Override
    public Badge findByCode(String code) {
        return null;
    }
}
