package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.*;
import org.sid.gmao_back.repositories.*;
import org.sid.gmao_back.service.VehiculeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class VehiculeServiceImpl implements VehiculeService {

    @Autowired
    private VehiculeRepository vehiculeRepository;
    @Autowired
    private TypeAssuranceRepository typeAssuranceRepository;
    @Autowired
    private CategorieVehiculeRepository categorieVehiculeRepository;
    @Autowired
    private ZoneRepository zoneRepository;
    @Autowired
    private TypeContratRepository typeContratRepository;
    @Autowired
    private TypeBadgeRepository typeBadgeRepository;
    @Override
    public List<Vehicule> findAll() {
        return vehiculeRepository.findAll();
    }

    @Override
    public Vehicule findBydId(Long id) {
        return vehiculeRepository.findById(id).get();
    }

    @Override
    public Vehicule save(Vehicule vehicule) {
        return vehiculeRepository.save(vehicule);
    }

    @Override
    public Vehicule update(Long id, Vehicule vehicule) {
        vehicule.setId(id);
        return vehiculeRepository.save(vehicule);
    }

    @Override
    public void delete(Long id) {
        vehiculeRepository.deleteById(id);

    }

    @Override
    public List<TypeAssurance> typeAssurance() {
            return (List<TypeAssurance>) typeAssuranceRepository.findAll();
    }

    @Override
    public List<CategorieVehicule> categorieVehicule() {
        return (List<CategorieVehicule>) categorieVehiculeRepository.findAll();
    }

    @Override
    public List<TypeBadge> typeBadge() {
        return (List<TypeBadge>) typeBadgeRepository.findAll();
    }

    @Override
    public List<TypeContrat> typeContrat() {
        return (List<TypeContrat>) typeContratRepository.findAll();
    }

    @Override
    public List<Zone> zone() {
        return (List<Zone>) zoneRepository.findAll();
    }

    @Override
    public Vehicule findByCode(String code) {
        return null;
    }
}
