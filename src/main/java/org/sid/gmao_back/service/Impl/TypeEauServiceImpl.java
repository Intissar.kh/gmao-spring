package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.TypeEau;
import org.sid.gmao_back.repositories.TypeEauRepository;
import org.sid.gmao_back.service.TypeEauService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeEauServiceImpl implements TypeEauService {

    @Autowired
    private TypeEauRepository typeEauRepository;
    @Override
    public List<TypeEau> findAll() {
        return typeEauRepository.findAll();
    }

    @Override
    public TypeEau findById(Long id) {
        return typeEauRepository.findById(id).get();
    }

    @Override
    public TypeEau save(TypeEau typeEau) {
        return typeEauRepository.save(typeEau);
    }

    @Override
    public TypeEau update(Long id, TypeEau typeEau) {
        typeEau.setId(id);
        return typeEauRepository.save(typeEau);
    }

    @Override
    public void delete(Long id) {
        typeEauRepository.deleteById(id);
    }

    @Override
    public TypeEau findByCode(String code) {
        return typeEauRepository.findByCode(code);
    }
}
