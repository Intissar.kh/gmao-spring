package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.BadgeChauffeur;
import org.sid.gmao_back.entities.Chauffeur;
import org.sid.gmao_back.entities.TypeBadge;
import org.sid.gmao_back.repositories.BadgeChauffeurRepository;
import org.sid.gmao_back.repositories.ChauffeurRepository;
import org.sid.gmao_back.repositories.TypeBadgeRepository;
import org.sid.gmao_back.service.BadgeChauffeurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BadgeChauffeurServiceImpl implements BadgeChauffeurService {

    @Autowired
    private BadgeChauffeurRepository badgeChauffeurRepository;
    @Autowired
    private ChauffeurRepository chauffeurRepository;
    @Autowired
    private TypeBadgeRepository typeBadgeRepository;
    @Override
    public List<BadgeChauffeur> findAll() {
        return badgeChauffeurRepository.findAll();
    }

    @Override
    public BadgeChauffeur findBydId(Long id) {
        return badgeChauffeurRepository.findById(id).get();
    }

    @Override
    public BadgeChauffeur save(BadgeChauffeur badgeChauffeur) {
        return badgeChauffeurRepository.save(badgeChauffeur);
    }

    @Override
    public BadgeChauffeur update(Long id, BadgeChauffeur badgeChauffeur) {
        badgeChauffeur.setId(id);
        return badgeChauffeurRepository.save(badgeChauffeur);
    }

    @Override
    public void delete(Long id) {
       badgeChauffeurRepository.deleteById(id);
    }

    @Override
    public List<Chauffeur> chauffeur() {
        return (List<Chauffeur>) chauffeurRepository.findAll();
    }

    @Override
    public List<TypeBadge> typeBadge() {
        return (List<TypeBadge>) typeBadgeRepository.findAll();
    }
}
