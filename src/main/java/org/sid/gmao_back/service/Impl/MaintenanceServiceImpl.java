package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.*;
import org.sid.gmao_back.repositories.*;
import org.sid.gmao_back.service.MaintenanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MaintenanceServiceImpl implements MaintenanceService {

    @Autowired
    private MaintenanceRepository maintenanceRepository;
    @Autowired
    private VehiculeRepository vehiculeRepository;
    @Autowired
    private TypePanneRepository typePanneRepository;
    @Autowired
    private PieceRepository pieceRepository;
    @Autowired
    private TypeHuileRepository typeHuileRepository;
    @Autowired
    private TypeEauRepository typeEauRepository;
    @Autowired
    private EtatMaintenanceRepository etatMaintenanceRepository;
    @Autowired
    private TypeMaintenanceRepository typeMaintenanceRepository;

    @Override
    public List<Maintenance> findAll() {
        return maintenanceRepository.findAll();
    }

    @Override
    public Maintenance findById(Long id) {
        return maintenanceRepository.findById(id).get();
    }

    @Override
    public Maintenance save(Maintenance maintenance) {
        return maintenanceRepository.save(maintenance);
    }

    @Override
    public Maintenance update(Long id, Maintenance maintenance) {
        maintenance.setId(id);
        return maintenanceRepository.save(maintenance);
    }

    @Override
    public void delete(Long id) {
   maintenanceRepository.deleteById(id);

    }

    @Override
    public List<Vehicule> vehicule() {
        return (List<Vehicule>) vehiculeRepository.findAll();
    }

    @Override
    public List<TypePanne> typePanne() {
        return (List<TypePanne>) typePanneRepository.findAll();
    }

    @Override
    public List<Piece> typePiece() {
        return (List<Piece>) pieceRepository.findAll();
    }

    @Override
    public List<TypeHuile> typeHuile() {
        return (List<TypeHuile>) typeHuileRepository.findAll();
    }

    @Override
    public List<TypeEau> typeEau() {
        return (List<TypeEau>) typeEauRepository.findAll();
    }

    @Override
    public List<EtatMaintenance> etatMaintenance() {
        return (List<EtatMaintenance>) etatMaintenanceRepository.findAll();
    }

    @Override
    public List<TypeMaintenance> typeMaintenance() {
        return (List<TypeMaintenance>) typeMaintenanceRepository.findAll();
    }
}
