package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.CatalogueTransport;
import org.sid.gmao_back.repositories.CatalogueTransportRepository;
import org.sid.gmao_back.service.CatalogueTransportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CatalogueTransportServiceImpl implements CatalogueTransportService {


    @Autowired
    private CatalogueTransportRepository catalogueTransportRepository;
    @Override
    public List<CatalogueTransport> findAll() {
        return catalogueTransportRepository.findAll();
    }

    @Override
    public CatalogueTransport findById(Long id) {
        return catalogueTransportRepository.findById(id).get();
    }

    @Override
    public CatalogueTransport save(CatalogueTransport catalogueTransport) {
        return catalogueTransportRepository.save(catalogueTransport);
    }

    @Override
    public CatalogueTransport update(Long id, CatalogueTransport catalogueTransport) {
        catalogueTransport.setId(id);
        return catalogueTransportRepository.save(catalogueTransport);
    }

    @Override
    public void delete(Long id) {
        catalogueTransportRepository.deleteById(id);

    }
}
