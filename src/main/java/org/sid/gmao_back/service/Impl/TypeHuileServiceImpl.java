package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.TypeHuile;
import org.sid.gmao_back.repositories.TypeHuileRepository;
import org.sid.gmao_back.service.TypeHuileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeHuileServiceImpl implements TypeHuileService {

    @Autowired
    private TypeHuileRepository typeHuileRepository;

    @Override
    public List<TypeHuile> findAll() {
        return typeHuileRepository.findAll();
    }

    @Override
    public TypeHuile findById(Long id) {
        return typeHuileRepository.findById(id).get();
    }

    @Override
    public TypeHuile save(TypeHuile typeHuile) {
        return typeHuileRepository.save(typeHuile);
    }

    @Override
    public TypeHuile update(Long id, TypeHuile typeHuile) {
        typeHuile.setId(id);
        return typeHuileRepository.save(typeHuile);
    }

    @Override
    public void delete(Long id) {
        typeHuileRepository.deleteById(id);
    }

    @Override
    public TypeHuile findByCode(String code) {
        return typeHuileRepository.findByCode(code);
    }
}
