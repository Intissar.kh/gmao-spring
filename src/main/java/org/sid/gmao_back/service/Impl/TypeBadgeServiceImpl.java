package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.TypeBadge;
import org.sid.gmao_back.entities.TypeContrat;
import org.sid.gmao_back.repositories.TypeBadgeRepository;
import org.sid.gmao_back.repositories.TypeContratRepository;
import org.sid.gmao_back.service.TypeBadgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeBadgeServiceImpl implements TypeBadgeService {
    @Autowired
    private TypeBadgeRepository typeBadgeRepository;

    @Override
    public List<TypeBadge> findAll() {
        return typeBadgeRepository.findAll();
    }

    @Override
    public TypeBadge findById(Long id) {
        return typeBadgeRepository.findById(id).get();
    }
    @Override
    public TypeBadge save(TypeBadge typeBadge) {
        return typeBadgeRepository.save(typeBadge);
    }

    @Override
    public TypeBadge update(Long id, TypeBadge typeBadge) {
        typeBadge.setId(id);
        return typeBadgeRepository.save(typeBadge);
    }

    @Override
    public void delete(Long id) {
        typeBadgeRepository.deleteById(id);

    }

    @Override
    public TypeBadge findByCode(String code) {
        return typeBadgeRepository.findByCode(code);
    }

}
