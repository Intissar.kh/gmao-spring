package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.EtatMaintenance;
import org.sid.gmao_back.exceptions.AttributesNotFound;
import org.sid.gmao_back.exceptions.ErrorType;
import org.sid.gmao_back.exceptions.IdNotFound;
import org.sid.gmao_back.repositories.EtatMaintenanceRepository;
import org.sid.gmao_back.service.EtatMaintenanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class EtatMaintenanceServiceImpl implements EtatMaintenanceService {
    @Autowired
    private EtatMaintenanceRepository etatMaintenanceRepository;

    @Override
    public List<EtatMaintenance> findAll() {
        return etatMaintenanceRepository.findAll();
    }

    @Override
    public EtatMaintenance findById(long id) {
        return etatMaintenanceRepository.findById(id).get();
    }


    @Override
    public EtatMaintenance update(long id, EtatMaintenance etatMaintenance) {
        etatMaintenance.setId(id);
        return etatMaintenanceRepository.save(etatMaintenance);
    }

    @Override
    public EtatMaintenance save(EtatMaintenance etatMaintenance) {
        return etatMaintenanceRepository.save(etatMaintenance);
    }

    @Override
    public void delete(long id) {
     etatMaintenanceRepository.deleteById(id);
    }
}
