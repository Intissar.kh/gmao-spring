package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.Chauffeur;
import org.sid.gmao_back.repositories.ChauffeurRepository;
import org.sid.gmao_back.service.ChauffeurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChauffeurServiceImpl implements ChauffeurService {

    @Autowired
    private ChauffeurRepository chauffeurRepository;
    @Override
    public List<Chauffeur> findAll() {
        return chauffeurRepository.findAll();
    }

    @Override
    public Chauffeur findById(Long id) {
        return chauffeurRepository.findById(id).get();
    }

    @Override
    public Chauffeur save(Chauffeur chauffeur) {
        return chauffeurRepository.save(chauffeur);
    }

    @Override
    public Chauffeur update(Long id, Chauffeur chauffeur) {
        chauffeur.setId(id);
        return chauffeurRepository.save(chauffeur);
    }

    @Override
    public void delete(Long id) {
        chauffeurRepository.deleteById(id);
    }

    @Override
    public Chauffeur findByCode(String code) {
        return null;
    }
}
