package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.Zone;
import org.sid.gmao_back.exceptions.IdNotFound;
import org.sid.gmao_back.repositories.ZoneRepository;
import org.sid.gmao_back.service.ZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ZoneServiceImpl implements ZoneService {

    @Autowired
    private ZoneRepository zoneRepository;
    @Override
    public List<Zone> findAll() {
        return zoneRepository.findAll();
    }

    @Override
    public Zone findById(Long id)  {
        return zoneRepository.findById(id).get();
    }

    @Override
    public Zone save(Zone zone) {
        return zoneRepository.save(zone);
    }

    @Override
    public Zone update(Long id, Zone zone) {
        zone.setId(id);
        return zoneRepository.save(zone);
    }

    @Override
    public void delete(Long id) {
        zoneRepository.deleteById(id);
    }
}
