package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.Assurance;
import org.sid.gmao_back.exceptions.IdNotFound;
import org.sid.gmao_back.repositories.AssuranceRepository;
import org.sid.gmao_back.service.AssuranceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AssuranceServiceImpl implements AssuranceService {
    @Autowired
    private AssuranceRepository assuranceRepository;

    @Override
    public List<Assurance> findAll() {
        return assuranceRepository.findAll();
    }

    @Override
    public Assurance findById(Long id) throws IdNotFound {
        return assuranceRepository.findById(id).get();
    }

    @Override
    public Assurance findByCode(String code) {
        return assuranceRepository.findByCode(code);
    }

    @Override
    public Assurance update(Long id, Assurance assurance) {
        assurance.setId(id);
        return assuranceRepository.save(assurance);
    }

    @Override
    public Assurance save(Assurance assurance) {
        return assuranceRepository.save(assurance);
    }

    @Override
    public void delete(long id) {
        assuranceRepository.deleteById(id);
    }
}
