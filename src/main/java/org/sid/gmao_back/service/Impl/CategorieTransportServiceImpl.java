package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.CategorieTransport;
import org.sid.gmao_back.entities.CategorieVehicule;
import org.sid.gmao_back.repositories.CategorieTrnasportRepository;
import org.sid.gmao_back.repositories.CategorieVehiculeRepository;
import org.sid.gmao_back.service.CategorieTransportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategorieTransportServiceImpl implements CategorieTransportService {

    @Autowired
    private CategorieTrnasportRepository categorieTrnasportRepository;
    @Override
    public List<CategorieTransport> findAll() {
        return categorieTrnasportRepository.findAll() ;
    }

    @Override
    public CategorieTransport findById(Long id) {
        return categorieTrnasportRepository.findById(id).get();
    }

    @Override
    public CategorieTransport save(CategorieTransport categorieTransport) {
        return categorieTrnasportRepository.save(categorieTransport);
    }

    @Override
    public CategorieTransport update(Long id, CategorieTransport categorieTransport) {
        categorieTransport.setId(id);
        return categorieTrnasportRepository.save(categorieTransport);
    }

    @Override
    public void delete(Long id) {
        categorieTrnasportRepository.deleteById(id);
    }
}
