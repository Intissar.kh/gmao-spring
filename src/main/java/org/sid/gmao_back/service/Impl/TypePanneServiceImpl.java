package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.TypePanne;
import org.sid.gmao_back.repositories.TypePanneRepository;
import org.sid.gmao_back.service.TypePanneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypePanneServiceImpl implements TypePanneService {

    @Autowired
    private TypePanneRepository typePanneRepository;
    @Override
    public List<TypePanne> findAll() {
        return typePanneRepository.findAll();
    }

    @Override
    public TypePanne findById(Long id) {
        return typePanneRepository.findById(id).get();
    }

    @Override
    public TypePanne save(TypePanne typePanne) {
        return typePanneRepository.save(typePanne);
    }

    @Override
    public TypePanne update(Long id, TypePanne typePanne) {
        typePanne.setId(id);
        return typePanneRepository.save(typePanne);
    }

    @Override
    public void delete(Long id) {
        typePanneRepository.deleteById(id);
    }
}
