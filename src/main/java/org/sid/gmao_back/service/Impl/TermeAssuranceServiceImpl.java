package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.TermeAssurance;
import org.sid.gmao_back.repositories.TermeAssuranceRepository;
import org.sid.gmao_back.service.TermeAssuranceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.AccessType;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TermeAssuranceServiceImpl implements TermeAssuranceService {

    @Autowired
    private TermeAssuranceRepository termeAssuranceRepository;
    @Override
    public List<TermeAssurance> findAll() {
        return termeAssuranceRepository.findAll();
    }

    @Override
    public TermeAssurance findById(Long id) {
        return termeAssuranceRepository.findById(id).get();
    }

    @Override
    public TermeAssurance findByCode(String code) {
        return termeAssuranceRepository.findByCode(code);
    }

    @Override
    public TermeAssurance save(TermeAssurance termeAssurance) {
        return termeAssuranceRepository.save(termeAssurance);
    }

    @Override
    public TermeAssurance update(Long id, TermeAssurance termeAssurance) {
        termeAssurance.setId(id);
        return termeAssuranceRepository.save(termeAssurance);
    }

    @Override
    public void delete(Long id) {
        termeAssuranceRepository.deleteById(id);

    }
}
