package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.EtatMaintenance;
import org.sid.gmao_back.entities.TypeMaintenance;
import org.sid.gmao_back.repositories.TypeMaintenanceRepository;
import org.sid.gmao_back.service.TypeMaintenanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeMaintenanceServiceImpl implements TypeMaintenanceService {


    @Autowired
     private TypeMaintenanceRepository typeMaintenanceRepository;
    @Override
    public List<TypeMaintenance> findAll() {
        return typeMaintenanceRepository.findAll();
    }

    @Override
    public TypeMaintenance findById(Long id) {
        return typeMaintenanceRepository.findById(id).get();
    }


    @Override
    public TypeMaintenance save(TypeMaintenance typeMaintenance) {
        return typeMaintenanceRepository.save(typeMaintenance);
    }

    @Override
    public TypeMaintenance update(Long id, TypeMaintenance typeMaintenance) {
        typeMaintenance.setId(id);
        return typeMaintenanceRepository.save(typeMaintenance);
    }

    @Override
    public void delete(Long id) {
        typeMaintenanceRepository.deleteById(id);
    }
}
