package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.AppRole;
import org.sid.gmao_back.entities.AppUser;
import org.sid.gmao_back.repositories.AppRoleRepository;
import org.sid.gmao_back.repositories.AppUserRepository;
import org.sid.gmao_back.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AppUserRepository appUserRepository;
    @Autowired
    private AppRoleRepository appRoleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Override
    public AppUser saveUser(AppUser user) {
        String password=bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(password);
        return appUserRepository.save(user);
    }

    @Override
    public List<AppUser> findAll() {
        return appUserRepository.findAll();
    }

    @Override
    public AppUser findById(Long id) {
        return appUserRepository.findById(id).get();
    }

    @Override
    public String findUsername(String user) {
        return  appUserRepository.findByUsername(user).getUsername();
    }

    @Override
    public String findPassword(String password) {
        return  appUserRepository.findByPassword(password).getPassword();
    }


    @Override
    public AppUser update(Long id, AppUser user) {
        user.setId(id);
        return appUserRepository.save(user);
    }

    @Override
    public AppRole saveRole(AppRole role) {
        return appRoleRepository.save(role);
    }

    @Override
    public void addRoleToUser(String username, String role) {
        AppRole appRole=appRoleRepository.findByRoleName(role);
        AppUser appUser=appUserRepository.findByUsername(username);
       // appUser.getRoles().add(appRole);

    }

    @Override
    public AppUser findUserByUsername(String username) {
        return appUserRepository.findByUsername(username);
    }
}
