package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.Piece;
import org.sid.gmao_back.repositories.PieceRepository;
import org.sid.gmao_back.service.PieceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PieceServiceImpl implements PieceService {
    @Autowired
    private PieceRepository pieceRepository;

    @Override
    public List<Piece> findAll() {
        return pieceRepository.findAll();
    }

    @Override
    public Piece findById(Long id) {
        return pieceRepository.findById(id).get();
    }

    @Override
    public Piece save(Piece piece) {
        return pieceRepository.save(piece);
    }

    @Override
    public Piece update(Long id, Piece piece) {
        piece.setId(id);
        return pieceRepository.save(piece);
    }

    @Override
    public void delete(Long id) {
        pieceRepository.deleteById(id);
    }
}
