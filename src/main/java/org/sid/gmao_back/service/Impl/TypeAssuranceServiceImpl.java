package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.TermeAssurance;
import org.sid.gmao_back.entities.TypeAssurance;
import org.sid.gmao_back.exceptions.IdNotFound;
import org.sid.gmao_back.repositories.TypeAssuranceRepository;
import org.sid.gmao_back.service.TypeAssuranceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeAssuranceServiceImpl implements TypeAssuranceService {
    @Autowired
    private TypeAssuranceRepository typeAssuranceRepository;
    @Override
    public List<TypeAssurance> findAll() {
        return typeAssuranceRepository.findAll();
    }

    @Override
    public TypeAssurance findById(Long id) throws IdNotFound {
        return typeAssuranceRepository.findById(id).get();
    }

    @Override
    public TermeAssurance findByCode(String code) {
        return typeAssuranceRepository.findByCode(code);
    }

    @Override
    public TypeAssurance save(TypeAssurance typeAssurance) {
        return typeAssuranceRepository.save(typeAssurance);
    }

    @Override
    public TypeAssurance update(Long id, TypeAssurance typeAssurance) {
        typeAssurance.setId(id);
        return typeAssuranceRepository.save(typeAssurance);
    }

    @Override
    public void delete(Long id) {
        typeAssuranceRepository.deleteById(id);

    }
}
