package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.TypeContrat;
import org.sid.gmao_back.repositories.TypeContratRepository;
import org.sid.gmao_back.service.TypeContratService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeContratServiceImpl implements TypeContratService {
    @Autowired
    private TypeContratRepository typeContratRepository;

    @Override
    public List<TypeContrat> findAll() {
        return typeContratRepository.findAll();
    }

    @Override
    public TypeContrat findById(Long id) {
        return typeContratRepository.findById(id).get();
    }


    @Override
    public TypeContrat save(TypeContrat typeContrat) {
        return typeContratRepository.save(typeContrat);
    }

    @Override
    public TypeContrat update(Long id, TypeContrat typeContrat) {
        typeContrat.setId(id);
        return typeContratRepository.save(typeContrat);
    }

    @Override
    public void delete(Long id) {
        typeContratRepository.deleteById(id);

    }

    @Override
    public TypeContrat findByCode(String code) {
        return typeContratRepository.findByCode(code);
    }

}
