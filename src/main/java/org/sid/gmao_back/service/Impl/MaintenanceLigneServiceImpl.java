package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.EtatMaintenance;
import org.sid.gmao_back.entities.MaintenanceLigne;
import org.sid.gmao_back.exceptions.IdNotFound;
import org.sid.gmao_back.repositories.MaintenanceLogneRepository;
import org.sid.gmao_back.service.MaintenanceLigneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MaintenanceLigneServiceImpl implements MaintenanceLigneService {

    @Autowired
    private MaintenanceLogneRepository maintenanceLogneRepository;
    @Override
    public List<MaintenanceLigne> findAll() {
        return maintenanceLogneRepository.findAll();
    }

    @Override
    public MaintenanceLigne findById(Long id) throws IdNotFound {
        return maintenanceLogneRepository.findById(id).get();
    }

    @Override
    public MaintenanceLigne update(Long id, MaintenanceLigne maintenanceLigne) {
        maintenanceLigne.setId(id);
        return maintenanceLogneRepository.save(maintenanceLigne);
    }

    @Override
    public MaintenanceLigne save(MaintenanceLigne maintenanceLigne) {
        return maintenanceLogneRepository.save(maintenanceLigne);
    }

    @Override
    public void delete(long id) {
        maintenanceLogneRepository.deleteById(id);
    }
}
