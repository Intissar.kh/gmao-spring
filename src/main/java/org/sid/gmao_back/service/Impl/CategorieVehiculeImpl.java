package org.sid.gmao_back.service.Impl;

import org.sid.gmao_back.entities.Badge;
import org.sid.gmao_back.entities.CategorieVehicule;
import org.sid.gmao_back.entities.TypeAssurance;
import org.sid.gmao_back.repositories.CategorieVehiculeRepository;
import org.sid.gmao_back.repositories.TypeAssuranceRepository;
import org.sid.gmao_back.service.CategorieVehiculeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategorieVehiculeImpl implements CategorieVehiculeService {
    @Autowired
    private CategorieVehiculeRepository categorieVehiculeRepository;
    @Autowired
    private TypeAssuranceRepository typeAssuranceRepository;

    @Override
    public List<CategorieVehicule> findAll() {
        return categorieVehiculeRepository.findAll();
    }

    @Override
    public CategorieVehicule findBydId(Long id) {
        return categorieVehiculeRepository.findById(id).get();
    }

    @Override
    public CategorieVehicule save(CategorieVehicule categorieVehicule) {
        return categorieVehiculeRepository.save(categorieVehicule);
    }

    @Override
    public CategorieVehicule update(Long id, CategorieVehicule categorieVehicule) {
        categorieVehicule.setId(id);
        return categorieVehiculeRepository.save(categorieVehicule);
    }

    @Override
    public void delete(Long id) {
        categorieVehiculeRepository.deleteById(id);

    }

    @Override
    public List<TypeAssurance> typeAssurance() {
        return (List<TypeAssurance>) typeAssuranceRepository.findAll();
    }

    @Override
    public CategorieVehicule findByCode(String code) {
        return categorieVehiculeRepository.findByCode(code);
    }

}
