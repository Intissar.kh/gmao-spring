package org.sid.gmao_back.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "maintenance_ligne")
@Data @AllArgsConstructor @NoArgsConstructor @ToString
public class MaintenanceLigne implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long id;
    private double kmJour;
    private double litreGasoilJour;
    private double coutEau;
    private double coutHuile;
    private double coutVehicule;

}
