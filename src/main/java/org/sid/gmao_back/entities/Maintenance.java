package org.sid.gmao_back.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name = "maintenance")
public class Maintenance {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "idVehicule")
    private Vehicule vehicule;
    private Date dateDebut;
    private Date dateFin;
    private String heureIntervantion;
    @ManyToOne
    @JoinColumn(name = "idTypeMaintenance")
    private TypeMaintenance typeMaintenance;
    @ManyToOne
    @JoinColumn(name = "idEtatMaintenance")
    private EtatMaintenance etatMaintenance;
    @ManyToOne
    @JoinColumn(name = "idTypePanne")
    private TypePanne typePanne;
    @ManyToOne
    @JoinColumn(name = "idPiece")
    private Piece piece;
    private Number qtePiece;
    private Number puPiece;
    private Number coutPiece;
    private Number kmSortie;
    private Number kmRetour;
    private Number gasoilParJour;
    private Number puGasoil;
    @ManyToOne
    @JoinColumn(name = "idTypeHuile")
    private TypeHuile typeHuile;
    private Number qteHuile;
    private Number puHuile;
    private Number coutHuile;
    @ManyToOne
    @JoinColumn(name = "idTypeEau")
    private TypeEau typeEau;
    private Number qteEau;
    private Number puEau;
    private Number coutEau;
    private Number kmParJour;
    private Number litreGasoilParJour;
    private Number coutEauParJour;
    private Number coutHuileParJour;
    private Number coutVehiculeParJour;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Vehicule getVehicule() {
        return vehicule;
    }

    public void setVehicule(Vehicule vehicule) {
        this.vehicule = vehicule;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public String getHeureIntervantion() {
        return heureIntervantion;
    }

    public void setHeureIntervantion(String heureIntervantion) {
        this.heureIntervantion = heureIntervantion;
    }

    public TypeMaintenance getTypeMaintenance() {
        return typeMaintenance;
    }

    public void setTypeMaintenance(TypeMaintenance typeMaintenance) {
        this.typeMaintenance = typeMaintenance;
    }

    public EtatMaintenance getEtatMaintenance() {
        return etatMaintenance;
    }

    public void setEtatMaintenance(EtatMaintenance etatMaintenance) {
        this.etatMaintenance = etatMaintenance;
    }

    public TypePanne getTypePanne() {
        return typePanne;
    }

    public void setTypePanne(TypePanne typePanne) {
        this.typePanne = typePanne;
    }

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    public Number getQtePiece() {
        return qtePiece;
    }

    public void setQtePiece(Number qtePiece) {
        this.qtePiece = qtePiece;
    }

    public Number getPuPiece() {
        return puPiece;
    }

    public void setPuPiece(Number puPiece) {
        this.puPiece = puPiece;
    }

    public Number getCoutPiece() {
        return coutPiece;
    }

    public void setCoutPiece(Number coutPiece) {
        this.coutPiece = coutPiece;
    }

    public Number getKmSortie() {
        return kmSortie;
    }

    public void setKmSortie(Number kmSortie) {
        this.kmSortie = kmSortie;
    }

    public Number getKmRetour() {
        return kmRetour;
    }

    public void setKmRetour(Number kmRetour) {
        this.kmRetour = kmRetour;
    }

    public Number getGasoilParJour() {
        return gasoilParJour;
    }

    public void setGasoilParJour(Number gasoilParJour) {
        this.gasoilParJour = gasoilParJour;
    }

    public Number getPuGasoil() {
        return puGasoil;
    }

    public void setPuGasoil(Number puGasoil) {
        this.puGasoil = puGasoil;
    }

    public TypeHuile getTypeHuile() {
        return typeHuile;
    }

    public void setTypeHuile(TypeHuile typeHuile) {
        this.typeHuile = typeHuile;
    }

    public Number getQteHuile() {
        return qteHuile;
    }

    public void setQteHuile(Number qteHuile) {
        this.qteHuile = qteHuile;
    }

    public Number getPuHuile() {
        return puHuile;
    }

    public void setPuHuile(Number puHuile) {
        this.puHuile = puHuile;
    }

    public Number getCoutHuile() {
        return coutHuile;
    }

    public void setCoutHuile(Number coutHuile) {
        this.coutHuile = coutHuile;
    }

    public TypeEau getTypeEau() {
        return typeEau;
    }

    public void setTypeEau(TypeEau typeEau) {
        this.typeEau = typeEau;
    }

    public Number getQteEau() {
        return qteEau;
    }

    public void setQteEau(Number qteEau) {
        this.qteEau = qteEau;
    }

    public Number getPuEau() {
        return puEau;
    }

    public void setPuEau(Number puEau) {
        this.puEau = puEau;
    }

    public Number getCoutEau() {
        return coutEau;
    }

    public void setCoutEau(Number coutEau) {
        this.coutEau = coutEau;
    }

    public Number getKmParJour() {
        return kmParJour;
    }

    public void setKmParJour(Number kmParJour) {
        this.kmParJour = kmParJour;
    }

    public Number getLitreGasoilParJour() {
        return litreGasoilParJour;
    }

    public void setLitreGasoilParJour(Number litreGasoilParJour) {
        this.litreGasoilParJour = litreGasoilParJour;
    }

    public Number getCoutEauParJour() {
        return coutEauParJour;
    }

    public void setCoutEauParJour(Number coutEauParJour) {
        this.coutEauParJour = coutEauParJour;
    }

    public Number getCoutHuileParJour() {
        return coutHuileParJour;
    }

    public void setCoutHuileParJour(Number coutHuileParJour) {
        this.coutHuileParJour = coutHuileParJour;
    }

    public Number getCoutVehiculeParJour() {
        return coutVehiculeParJour;
    }

    public void setCoutVehiculeParJour(Number coutVehiculeParJour) {
        this.coutVehiculeParJour = coutVehiculeParJour;
    }
}
