package org.sid.gmao_back.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "catalogue_Transport")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CatalogueTransport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private BigDecimal HT;
    private BigDecimal TTC;
    private BigDecimal TVA;
}
