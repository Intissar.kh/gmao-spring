package org.sid.gmao_back.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "type_eau")
@Data @AllArgsConstructor @NoArgsConstructor @ToString
public class TypeEau {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long  id;
    private String code;
    private String description;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "typeEau",fetch = FetchType.LAZY)
    private List<Maintenance> maintenances=new ArrayList<>();

    @JsonIgnore
    public List<Maintenance> getMaintenances() { return maintenances; }

    public void setMaintenances(List<Maintenance> maintenances) { this.maintenances = maintenances; }
}

