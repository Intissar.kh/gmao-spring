package org.sid.gmao_back.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "zone")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Zone implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nom;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "zoneSource",fetch = FetchType.LAZY)
    private List<Vehicule> vehicule =new ArrayList<>();
    @JsonIgnore
    public List<Vehicule> getVehicule() { return vehicule; }
    public void setVehicule(List<Vehicule> vehicule) { this.vehicule = vehicule; }

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "zoneDestination",fetch = FetchType.LAZY)
    private List<Vehicule> vehicules =new ArrayList<>();

    @JsonIgnore
    public List<Vehicule> getVehicules() { return vehicules; }
    public void setVehicules(List<Vehicule> vehicules) { this.vehicules = vehicules; }
}
