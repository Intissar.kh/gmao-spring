package org.sid.gmao_back.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "type_contrat")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TypeContrat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String code;
    private String description;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "typeContrat",fetch = FetchType.LAZY)
    private List<Vehicule> vehicule=new ArrayList<>();

    @JsonIgnore
    public List<Vehicule> getVehicule() { return vehicule; }
    public void setVehicule(List<Vehicule> vehicule) { this.vehicule = vehicule; }
}
