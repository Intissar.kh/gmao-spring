package org.sid.gmao_back.entities;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "vehicule")
@Data @NoArgsConstructor  @ToString
public class Vehicule {
 @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String immatriculation;
    private String vehiculeCode;
    @ManyToOne
    @JoinColumn(name = "categorieId")
    private CategorieVehicule categorieVehicule;
    @ManyToOne
    private Zone zoneSource;
    @ManyToOne
    private Zone zoneDestination;
    @ManyToOne
    @JoinColumn(name = "badgeId")
    private TypeBadge typeBadge;
    @ManyToOne
    @JoinColumn(name = "assuranceId")
    private TypeAssurance typeAssurance;
    private Date de;//date de debut d'assurance
    private Date a;// date de fin d'assurance
    private float montant; // montant d'assurance
    @ManyToOne
    @JoinColumn(name = "contratId")
    private TypeContrat typeContrat;
    private Date aquisitionDate;
    private float huileMoteur;
    private float portArriere;
    private float direction;
    private float radiator;
    private float filtreAir;
    private float boiteAvitesse;
    private float dessicance;
    private float carteGris;
    private float nCylindre;
    private float puissanceFiscal;
    private float corsVehicule;
    private float nChassis;
    private String energieVehicule;
    private Date  visiteTechnique;
    private float valeurVisiteThechnique;
    private Date vignette;
    private float valeurVignette;
   @OneToMany(cascade = CascadeType.ALL,mappedBy = "vehicule",fetch = FetchType.LAZY)
   private List<Maintenance> maintenances=new ArrayList<>();


   @JsonIgnore
   public List<Maintenance> getMaintenances() { return maintenances; }
   public void setMaintenances(List<Maintenance> maintenances) { this.maintenances = maintenances; }
}
