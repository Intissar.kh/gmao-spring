package org.sid.gmao_back.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "categorie_vehicule")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CategorieVehicule  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;
    private String code;
    private String description;
    private String poids;
    private double profondeur;
    private double tonnage;
    private double largeur;
    private double poidsVide;
    private double  poidsTotal;
    @ManyToOne
    @JoinColumn(name = "typeId")
    private TypeAssurance typeAssurance;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "categorieVehicule",fetch = FetchType.LAZY)
    private List<Vehicule> vehicules=new ArrayList<>();

    @JsonIgnore
    public List<Vehicule> getVehicules() { return vehicules; }
    public void setVehicules(List<Vehicule> vehicules) { this.vehicules = vehicules; }

    //@JsonManagedReference
    public TypeAssurance getTypeAssurance() { return typeAssurance; }
    public void setTypeAssurance(TypeAssurance typeAssurance) { this.typeAssurance = typeAssurance; }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getCode() { return code; }

    public void setCode(String code) { this.code = code; }

    public String getPoids() { return poids; }

    public void setPoids(String poids) { this.poids = poids; }

    public double getProfondeur() { return profondeur; }

    public void setProfondeur(double profondeur) { this.profondeur = profondeur; }

    public double getTonnage() { return tonnage; }

    public void setTonnage(double tonnage) { this.tonnage = tonnage; }

    public double getLargeur() { return largeur; }

    public void setLargeur(double largeur) { this.largeur = largeur; }

    public double getPoidsVide() { return poidsVide; }

    public void setPoidsVide(double poidsVide) { this.poidsVide = poidsVide; }

    public double getPoidsTotal() { return poidsTotal; }

    public void setPoidsTotal(double poidsTotal) { this.poidsTotal = poidsTotal; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }
}
