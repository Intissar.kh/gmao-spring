package org.sid.gmao_back.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "etat_maintenance")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class EtatMaintenance  {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String code;
    private String description;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "etatMaintenance",fetch = FetchType.LAZY)
    private List<Maintenance>  maintenances=new ArrayList<>();


    @JsonIgnore
    public List<Maintenance> getMaintenances() { return maintenances; }
    public void setMaintenances(List<Maintenance> maintenances) { this.maintenances = maintenances; }
}
