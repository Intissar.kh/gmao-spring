package org.sid.gmao_back.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Entity
@Table(name = "chauffeur")
public class Chauffeur {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String code;
    private String CIN;
    private String nom;
    private String prenom;
    private Date dateNaissance;
    private Date visiteMedicale;
    private String nCarte;
    private String nTelephone;
    private String nFax;
    private String email;
    private float montant;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "chauffeur",fetch = FetchType.LAZY)
    private List<BadgeChauffeur> badgeChauffeurs=new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCIN() {
        return CIN;
    }

    public void setCIN(String CIN) {
        this.CIN = CIN;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public Date getVisiteMedicale() {
        return visiteMedicale;
    }

    public void setVisiteMedicale(Date visiteMedicale) {
        this.visiteMedicale = visiteMedicale;
    }

    public String getnCarte() {
        return nCarte;
    }

    public void setnCarte(String nCarte) {
        this.nCarte = nCarte;
    }

    public String getnTelephone() {
        return nTelephone;
    }

    public void setnTelephone(String nTelephone) {
        this.nTelephone = nTelephone;
    }

    public String getnFax() {
        return nFax;
    }

    public void setnFax(String nFax) {
        this.nFax = nFax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public float getMontant() {
        return montant;
    }

    public void setMontant(float montant) {
        this.montant = montant;
    }

    @JsonIgnore
    public List<BadgeChauffeur> getBadgeChauffeurs() { return badgeChauffeurs; }

    public void setBadgeChauffeurs(List<BadgeChauffeur> badgeChauffeurs) { this.badgeChauffeurs = badgeChauffeurs; }
}
