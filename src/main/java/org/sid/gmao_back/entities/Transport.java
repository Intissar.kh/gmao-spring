package org.sid.gmao_back.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "transport")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class Transport  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private  String  code;
    private String nom;
    private String  adresse;
    private String description;
    private BigDecimal serieTransport;
    private String commentaire;
    private Boolean TransportValide;
}
