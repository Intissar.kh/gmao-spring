package org.sid.gmao_back.entities;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "type_assurance")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TypeAssurance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String code;
    private String description;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "typeAssurance",fetch = FetchType.LAZY)
    private List<CategorieVehicule> categorieVehicules = new ArrayList<>() ;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "typeAssurance",fetch = FetchType.LAZY)
    private List<Vehicule> vehicule=new ArrayList<>();

    @JsonIgnore
    public List<Vehicule> getVehicule() { return vehicule; }

    public void setVehicule(List<Vehicule> vehicule) { this.vehicule = vehicule; }

    @JsonIgnore
    public List<CategorieVehicule> getCategorieVehicules() { return categorieVehicules; }

    public void setCategorieVehicules(List<CategorieVehicule> categorieVehicules) { this.categorieVehicules = categorieVehicules; }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getCode() { return code;}

    public void setCode(String code) { this.code = code; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }
}
