package org.sid.gmao_back.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "terme_assurance")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TermeAssurance {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String code;
    private String Description;
}
