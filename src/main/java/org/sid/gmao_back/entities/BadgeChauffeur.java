package org.sid.gmao_back.entities;

import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name = "badgeChauffeur")
public class BadgeChauffeur {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date dateDelivrance;
    private Date dateFinValidite;
    private String nBadge;
    @ManyToOne
    @JoinColumn(name = "chauffeurId")
    private Chauffeur chauffeur;
    @ManyToOne
    @JoinColumn(name = "badgeId")
    private TypeBadge typeBadges;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateDelivrance() {
        return dateDelivrance;
    }

    public void setDateDelivrance(Date dateDelivrance) {
        this.dateDelivrance = dateDelivrance;
    }

    public Date getDateFinValidite() {
        return dateFinValidite;
    }

    public void setDateFinValidite(Date dateFinValidite) {
        this.dateFinValidite = dateFinValidite;
    }

    public String getnBadge() {
        return nBadge;
    }

    public void setnBadge(String nBadge) {
        this.nBadge = nBadge;
    }

    public Chauffeur getChauffeur() {
        return chauffeur;
    }

    public void setChauffeur(Chauffeur chauffeur) {
        this.chauffeur = chauffeur;
    }

    public TypeBadge getTypeBadge() {
        return typeBadges;
    }

    public void setTypeBadge(TypeBadge typeBadge) {
        this.typeBadges = typeBadge;
    }
}
