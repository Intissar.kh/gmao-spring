package org.sid.gmao_back.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.bytebuddy.asm.Advice;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "role")
@Data @AllArgsConstructor @NoArgsConstructor @ToString
public class AppRole {
    @Id @GeneratedValue
    private Long id;
    private String roleName;
}
