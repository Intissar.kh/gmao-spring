package org.sid.gmao_back;

import org.sid.gmao_back.entities.*;
import org.sid.gmao_back.repositories.CategorieVehiculeRepository;
import org.sid.gmao_back.repositories.EtatMaintenanceRepository;
import org.sid.gmao_back.repositories.TaskRepository;
import org.sid.gmao_back.repositories.TypeAssuranceRepository;
import org.sid.gmao_back.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.stream.Stream;

@SpringBootApplication
public class GmaoBackApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(GmaoBackApplication.class, args);
    }

    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private AccountService accountService;
    @Autowired
    private EtatMaintenanceRepository etatMaintenanceRepository;
    @Autowired
    private CategorieVehiculeRepository categorieVehiculeRepository;
    @Autowired
    private RepositoryRestConfiguration repositoryRestConfiguration;
    @Autowired
    private TypeAssuranceRepository typeAssuranceRepository;
    @Bean
    public BCryptPasswordEncoder getBCPE(){
        return new BCryptPasswordEncoder();
    }
    @Override
    public void run(String... args) throws Exception {

        repositoryRestConfiguration.exposeIdsFor(EtatMaintenance.class);
        repositoryRestConfiguration.exposeIdsFor(TypeMaintenance.class);
        repositoryRestConfiguration.exposeIdsFor(MaintenanceLigne.class);
        repositoryRestConfiguration.exposeIdsFor(TypeBadge.class);
        repositoryRestConfiguration.exposeIdsFor(TypeContrat.class);
        repositoryRestConfiguration.exposeIdsFor(TypeEau.class);
        repositoryRestConfiguration.exposeIdsFor(TypeHuile.class);
        repositoryRestConfiguration.exposeIdsFor(TypePanne.class);
        repositoryRestConfiguration.exposeIdsFor(TypeAssurance.class,CategorieVehicule.class);
        repositoryRestConfiguration.exposeIdsFor(Piece.class);
        repositoryRestConfiguration.exposeIdsFor(Zone.class);
        repositoryRestConfiguration.getCorsRegistry()
                .addMapping("/**")
                .allowedOrigins("*");
        TypeAssurance typeAssurance= new TypeAssurance(null,"ZD35","tous risque",null,null);
        typeAssuranceRepository.save(typeAssurance);

        categorieVehiculeRepository.save(new CategorieVehicule(null,"ggq","camion","sjfq",11.23,22.66,3.478,241.43,348.241,typeAssurance,null));
        etatMaintenanceRepository.save(new EtatMaintenance(null,"123AB","en court",null));
        etatMaintenanceRepository.save(new EtatMaintenance(null,"123AB","en court",null));
        etatMaintenanceRepository.save(new EtatMaintenance(null,"123AB","en court",null));
        etatMaintenanceRepository.save(new EtatMaintenance(null,"123AB","en court",null));
        etatMaintenanceRepository.save(new EtatMaintenance(null,"123AB","en court",null));
        etatMaintenanceRepository.save(new EtatMaintenance(null,"123AB","en court",null));
        etatMaintenanceRepository.save(new EtatMaintenance(null,"123AB","en court",null));
        etatMaintenanceRepository.save(new EtatMaintenance(null,"123AB","en court",null));
        etatMaintenanceRepository.save(new EtatMaintenance(null,"123AB","en court",null));
        Stream.of("fait").forEach(e->{
            etatMaintenanceRepository.save(new EtatMaintenance(null,"ARS123",e,null));
        });
        etatMaintenanceRepository.findAll().forEach(e->{
            System.out.println(e.getDescription());
        });
        accountService.saveUser(new AppUser(null,"intissar","1234","soumiya@gmail.com","TAZA",null));
        accountService.saveUser(new AppUser(null,"soumiya","1234","soumiya@gmail.com","TAZA",null));
        accountService.saveRole(new AppRole(null,"USER"));
        accountService.saveRole(new AppRole(null,"ADMIN"));
        accountService.addRoleToUser("admin","ADMIN");
        //accountService.addRoleToUser("admin","USER");
        accountService.addRoleToUser("user","USER");

        Stream.of("T1","T2","T3").forEach(t->{
            taskRepository.save(new Task(null,t));
        });
        taskRepository.findAll().forEach(t->{
            System.out.println(t.getTaskName());
        });
    }
}
