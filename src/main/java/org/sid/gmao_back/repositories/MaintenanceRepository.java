package org.sid.gmao_back.repositories;

import org.sid.gmao_back.entities.Maintenance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource
@CrossOrigin("*")
public interface MaintenanceRepository extends JpaRepository<Maintenance,Long> {

}
