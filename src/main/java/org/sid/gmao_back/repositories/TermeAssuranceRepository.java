package org.sid.gmao_back.repositories;

import org.sid.gmao_back.entities.CategorieVehicule;
import org.sid.gmao_back.entities.TermeAssurance;
import org.sid.gmao_back.entities.TypeHuile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("*")
@RepositoryRestResource
public interface TermeAssuranceRepository extends JpaRepository<TermeAssurance,Long> {
    @RestResource(path = "/byCodePage")
    public Page<TermeAssurance> findByCode(@Param("mc") String code, Pageable pageable);
    @RestResource(path = "/byCode")
    public List<TermeAssurance> findByCodeContains(@Param("mc") String code);
    @Query(value = "select tr from TermeAssurance tr where tr.code = ?1")
    TermeAssurance findByCode(String code);
}
