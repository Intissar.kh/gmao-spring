package org.sid.gmao_back.repositories;

import org.sid.gmao_back.entities.EtatMaintenance;
import org.sid.gmao_back.entities.MaintenanceLigne;
import org.sid.gmao_back.entities.TypeMaintenance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("*")
@RepositoryRestResource
public interface TypeMaintenanceRepository extends JpaRepository<TypeMaintenance,Long> {
    @RestResource(path = "/byCode")
    public List<TypeMaintenance> findByCodeContains(@Param("mc") String code);
    @RestResource(path = "/byCodePage")
    public Page<TypeMaintenance> findByCodeContains(@Param("mc") String code, Pageable pageable);
}
