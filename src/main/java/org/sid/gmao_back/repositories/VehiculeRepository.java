package org.sid.gmao_back.repositories;

import org.sid.gmao_back.entities.CategorieVehicule;
import org.sid.gmao_back.entities.Vehicule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
@RepositoryRestResource
@CrossOrigin("*")
public interface VehiculeRepository extends JpaRepository<Vehicule,Long> {
    @RestResource(path = "/byCode")
    public List<Vehicule> findByVehiculeCodeContains(@Param("mc") String code);
    @RestResource(path = "/byCodePage")
    public Page<Vehicule> findByVehiculeCodeContains(@Param("mc") String code, Pageable pageable);
    @Query(value = "select b from Vehicule  b where b.vehiculeCode = ?1")
    Vehicule findByCode(String vehiculeCode);
}
