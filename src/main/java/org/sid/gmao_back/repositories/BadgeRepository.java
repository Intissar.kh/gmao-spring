package org.sid.gmao_back.repositories;

import org.sid.gmao_back.entities.Badge;
import org.sid.gmao_back.entities.TermeAssurance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("*")
@RepositoryRestResource
public interface BadgeRepository extends JpaRepository<Badge,Long> {
    @RestResource(path = "/byPage")
    public Page<Badge> findByCode(@Param("mc") String code, Pageable pageable);
    @Query(value = "select b from Badge b where b.code = ?1")
    Badge findByCode(String code);
}
