package org.sid.gmao_back.repositories;

import org.sid.gmao_back.entities.CatalogueTransport;
import org.sid.gmao_back.entities.CategorieTransport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("*")
@RepositoryRestResource
public interface CatalogueTransportRepository extends JpaRepository<CatalogueTransport,Long> {
    @RestResource(path = "/byPage")
    public Page<CatalogueTransport> findById(@Param("mc") Long id, Pageable pageable);
}
