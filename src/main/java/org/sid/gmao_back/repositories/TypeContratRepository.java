package org.sid.gmao_back.repositories;

import org.sid.gmao_back.entities.TypeBadge;
import org.sid.gmao_back.entities.TypeContrat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("*")
@RepositoryRestResource
public interface TypeContratRepository extends JpaRepository<TypeContrat,Long> {
    @RestResource(path = "/byCode")
    public List<TypeContrat> findByCodeContains(@Param("mc") String code);
    @RestResource(path = "/byCodePage")
    public Page<TypeContrat> findByCodeContains(@Param("mc") String code, Pageable pageable);
    @Query(value = "select tc from TypeContrat tc where tc.code = ?1")
    TypeContrat findByCode(String code);
}
