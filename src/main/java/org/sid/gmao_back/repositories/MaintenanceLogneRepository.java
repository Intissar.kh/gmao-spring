package org.sid.gmao_back.repositories;

import org.sid.gmao_back.entities.EtatMaintenance;
import org.sid.gmao_back.entities.MaintenanceLigne;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("*")
@RepositoryRestResource
public interface MaintenanceLogneRepository extends JpaRepository<MaintenanceLigne,Long> {
    @RestResource(path = "/byCode")
    public List<MaintenanceLigne> findByIdContains(@Param("mc") Long id);
    @RestResource(path = "/byCodePage")
    public Page<MaintenanceLigne> findByIdContains(@Param("mc") Long id, Pageable pageable);
}

