package org.sid.gmao_back.repositories;

import org.sid.gmao_back.entities.TypeBadge;
import org.sid.gmao_back.entities.TypeContrat;
import org.sid.gmao_back.entities.TypeMaintenance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("*")
@RepositoryRestResource
public interface TypeBadgeRepository extends JpaRepository<TypeBadge,Long> {
    @RestResource(path = "/byCode")
    public List<TypeBadge> findByCodeContains(@Param("mc") String code);
    @RestResource(path = "/byCodePage")
    public Page<TypeBadge> findByCodeContains(@Param("mc") String code, Pageable pageable);
    @Query(value = "select tb from TypeBadge tb where tb.code = ?1")
    TypeBadge findByCode(String code);
}
