package org.sid.gmao_back.repositories;

import org.sid.gmao_back.entities.EtatMaintenance;
import org.sid.gmao_back.entities.TypePanne;
import org.sid.gmao_back.entities.Zone;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("*")
@RepositoryRestResource
public interface ZoneRepository extends JpaRepository<Zone,Long> {
    @RestResource(path = "/byCode")
    public List<Zone> findByNomContains(@Param("mc") String name);
    @RestResource(path = "/byCodePage")
    public Page<Zone> findByNomContains(@Param("mc") String name, Pageable pageable);
}
