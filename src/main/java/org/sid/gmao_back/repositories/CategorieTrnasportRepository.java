package org.sid.gmao_back.repositories;

import org.sid.gmao_back.entities.CategorieTransport;
import org.sid.gmao_back.entities.CategorieVehicule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

public interface CategorieTrnasportRepository extends JpaRepository<CategorieTransport,Long> {
    @RestResource(path = "/byPage")
    public Page<CategorieTransport> findById(@Param("mc") Long id, Pageable pageable);

}
