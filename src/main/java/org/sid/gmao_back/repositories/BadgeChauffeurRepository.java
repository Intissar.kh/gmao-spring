package org.sid.gmao_back.repositories;

import org.sid.gmao_back.entities.BadgeChauffeur;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("*")
@RepositoryRestResource
public interface BadgeChauffeurRepository extends JpaRepository<BadgeChauffeur,Long> {

}
