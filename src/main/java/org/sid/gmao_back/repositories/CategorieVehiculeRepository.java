package org.sid.gmao_back.repositories;


import org.sid.gmao_back.entities.Badge;
import org.sid.gmao_back.entities.CategorieVehicule;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("*")
@RepositoryRestResource
public interface CategorieVehiculeRepository extends JpaRepository<CategorieVehicule,Long> {
    @RestResource(path = "/byCode")
    public List<CategorieVehicule> findByCodeContains(@Param("mc") String code);
    @RestResource(path = "/byCodePage")
    public Page<CategorieVehicule> findByCodeContains(@Param("mc") String code, Pageable pageable);
   @Query(value = "select b from CategorieVehicule  b where b.code = ?1")
    CategorieVehicule findByCode(String code);

}
