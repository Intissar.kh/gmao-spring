package org.sid.gmao_back.repositories;

import org.sid.gmao_back.entities.TypeContrat;
import org.sid.gmao_back.entities.TypeEau;
import org.sid.gmao_back.entities.TypeHuile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("*")
@RepositoryRestResource
public interface TypeEauRepository extends JpaRepository<TypeEau,Long> {
    @RestResource(path = "/byCode")
    public List<TypeEau> findByCodeContains(@Param("mc") String code);
    @RestResource(path = "/byCodePage")
    public Page<TypeEau> findByCodeContains(@Param("mc") String code, Pageable pageable);
    @Query(value = "select te from TypeEau te where te.code = ?1")
    TypeEau findByCode(String code);
}
