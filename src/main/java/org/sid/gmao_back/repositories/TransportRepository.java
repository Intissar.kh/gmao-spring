package org.sid.gmao_back.repositories;

import org.sid.gmao_back.entities.EtatMaintenance;
import org.sid.gmao_back.entities.Piece;
import org.sid.gmao_back.entities.Transport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("*")
@RepositoryRestResource
public interface TransportRepository extends JpaRepository<Transport,Long> {
    @RestResource(path = "/byCode")
    public List<Transport> findByCodeContains(@Param("mc") String code);
    @RestResource(path = "/byDesignationPage")
    public Page<Transport> findByCodeContains(@Param("mc") String code, Pageable pageable);
    @Query(value = "select t from Transport t where t.code = ?1")
    Transport findByCode(String code);
}
