package org.sid.gmao_back.repositories;

import org.sid.gmao_back.entities.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface AppUserRepository extends JpaRepository<AppUser,Long> {
    AppUser findByUsername(String  username);
    AppUser findByPassword(String  password);

}
