package org.sid.gmao_back.repositories;


import org.sid.gmao_back.entities.Assurance;
import org.sid.gmao_back.entities.EtatMaintenance;
import org.sid.gmao_back.entities.TermeAssurance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("*")
@RepositoryRestResource
public interface AssuranceRepository  extends JpaRepository<Assurance,Long> {
    @RestResource(path = "/byCode")
    public List<Assurance> findByCodeContains(@Param("mc") String code);
    @RestResource(path = "/byCodePage")
    public Page<Assurance> findByCodeContains(@Param("mc") String code, Pageable pageable);
    @Query(value = "select a from Assurance a where a.code = ?1")
    Assurance findByCode(String code);

}
