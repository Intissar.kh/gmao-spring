package org.sid.gmao_back.repositories;

import org.sid.gmao_back.entities.MaintenanceLigne;
import org.sid.gmao_back.entities.Piece;
import org.sid.gmao_back.entities.TermeAssurance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("*")
@RepositoryRestResource
public interface PieceRepository extends JpaRepository<Piece,Long> {
    @RestResource(path = "/byCode")
    public List<Piece> findByPieceContains(@Param("mc") String piece);
    @RestResource(path = "/byCodePage")
    public Page<Piece> findByPieceContains(@Param("mc") String piece, Pageable pageable);

}
